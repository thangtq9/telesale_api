﻿using Hub_Pda.Api.Model.Zalo;
using Newtonsoft.Json;
using RestSharp;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace Hub_Pda.Api.Helps
{
    public class ZnsApi
    {
        public async Task<RestResponse> GetAccessToken(string refresh_token, string app_id, string secret_key)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var options = new RestClientOptions("https://oauth.zaloapp.com")
                {
                    MaxTimeout = -1
                };
                var client = new RestClient(options);
                var request = new RestRequest("/v4/oa/access_token?refresh_token=" + refresh_token + "&app_id=" + app_id + "&grant_type=refresh_token", Method.Post);
                request.AddHeader("secret_key", secret_key);
                RestResponse response = await client.ExecuteAsync(request);
                return response;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<zns_response_model> Send_sms_text(zns_response_model _rrm, string access_token, string cus_zalo_id, string mes)
        {
            zns_response_model rrm = _rrm;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var options = new RestClientOptions("https://openapi.zalo.me")
                {
                    MaxTimeout = -1,
                };
                var rm = new
                {
                    recipient = new
                    {
                        user_id = cus_zalo_id
                    },
                    message = new
                    {
                        text = mes
                    }

                };

                var client = new RestClient(options);
                var request = new RestRequest("/v3.0/oa/message/cs", Method.Post);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("access_token", access_token);
                request.AddJsonBody(rm);

                RestResponse rp_zns = await client.ExecuteAsync(request);
                if (rp_zns.StatusCode == HttpStatusCode.OK)
                {
                    zalo_response_sms_model zrsm = JsonConvert.DeserializeObject<zalo_response_sms_model>(rp_zns.Content);
                    if (zrsm != null && zrsm.error == 0)
                    {
                        rrm.status_code = "000";
                        rrm.msg = "Gửi sms thành công!";
                    }
                    else
                    {
                        rrm.status_code = "005";
                        rrm.msg = (zrsm == null ? "Lỗi gửi zns." : zrsm.message);
                    }
                }
                else
                {
                    rrm.status_code = "004";
                    rrm.msg = "Gửi zns lỗi!" + rp_zns.Content;
                }
            }
            catch (Exception ex)
            {
                rrm.status_code = "500";
                rrm.msg = ex.Message;
            }
            return rrm;
        }
        public async Task<zns_response_model> zns_noti(string url_zns, string order_no,int amount, string store_name,long purchasedate, string customer_name, string phone,string store_code, string bu)
        {
            zns_response_model rrm = new zns_response_model();
            try
            {
                var rm = new
                {
                    InvoiceNo = order_no,
                    InvoiceAmt = amount,
                    EInvoiceCode = "",
                    StoreName = store_name,
                    PurchaseDate = purchasedate,
                    CustId = "",
                    CustName = customer_name,
                    PhoneNo = phone,
                    StoreCode= store_code,
                    Bu = bu,
                    EinvoiceUrl ="",
                    DummyCard = false
                };
                var options = new RestClientOptions(url_zns)
                {
                    MaxTimeout = -1,
                };
                var client = new RestClient(options);
                var request = new RestRequest("/api/zns/send_noti?telesale=1", Method.Post);
                request.AddJsonBody(rm);

                RestResponse rp_zns = await client.ExecuteAsync(request);
                if (rp_zns.StatusCode == HttpStatusCode.OK)
                {
                    rrm = JsonConvert.DeserializeObject<zns_response_model>(rp_zns.Content);
                    if (rrm != null && rrm.error == 0)
                    {
                        rrm.status_code = "000";
                        rrm.msg = "Gửi sms thành công!";
                    }
                    else
                    {
                        rrm.status_code = "005";
                        rrm.msg = (rrm == null ? "Lỗi gửi zns." : rrm.msg);
                    }
                }
                else
                {
                    rrm.status_code = "004";
                    rrm.msg = "Gửi zns lỗi!" + rp_zns.Content;
                }
            }
            catch (Exception ex)
            {
                rrm.status_code = "500";
                rrm.msg = ex.Message;
            }
            return rrm;
        }
    }
}
