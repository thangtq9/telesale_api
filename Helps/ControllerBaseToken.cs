﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Web.Api.Model;

namespace Web.Api.Helps
{
    [Authorize]
    [ApiController]
    public class ControllerBaseToken : ControllerBase
    {
        public UserModel? Employee
        {
            get
            {
                try
                {
                    var user_info = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
                    if (user_info != null)
                        return JsonConvert.DeserializeObject<UserModel>(user_info);
                    else
                        return null;
                }
                catch (Exception)
                {

                    return null;
                }
            }
        }


        public List<MenuModel>? Menus
        {
            get
            {
                try
                {
                    var user_info = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
                    if (user_info != null)
                        return JsonConvert.DeserializeObject<List<MenuModel>>(user_info);
                    else
                        return null;
                }
                catch (Exception)
                {

                    return null;
                }
            }
        }

        public List<PermisstionButonModel>? Permissions
        {
            get
            {
                try
                {
                    var user_info = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value;
                    if (user_info != null)
                        return JsonConvert.DeserializeObject<List<PermisstionButonModel>>(user_info);
                    else
                        return null;
                }
                catch (Exception)
                {

                    return null;
                }
            }
        }
    }
}
