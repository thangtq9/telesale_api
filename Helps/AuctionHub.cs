﻿using Hub_Pda.Api.Model;
using Hub_Pda.Api.Services;
using Microsoft.AspNetCore.SignalR;

namespace Hub_Pda.Api.Helps
{
    public class AuctionHub : Hub
    {

        ///public static readonly List<User_hub> lconnectedClients = new List<User_hub>();

        private readonly OnlineUserService _onlineUserService;

        public AuctionHub(OnlineUserService onlineUserService)
        {
            _onlineUserService = onlineUserService;
        }

        public override async Task OnConnectedAsync()
        {
            var httpContext = Context.GetHttpContext();
            string DisplayName = "Random"; string EmployeeCode = "Random";
            if (httpContext != null)
            {
                DisplayName = httpContext.Request.Query["DisplayName"];
                EmployeeCode = httpContext.Request.Query["EmployeeCode"];
            }

            User_hub uh = new User_hub();
            uh.display_name = DisplayName;
            uh.employee_code = EmployeeCode;
            uh.connection_id = Context.ConnectionId;

            //lconnectedClients.Add(uh);
            _onlineUserService.RemoveUser(uh);
            _onlineUserService.AddUser(uh);
            await user_on(EmployeeCode,DisplayName);
            await send_list_user(_onlineUserService.GetOnlineUsers().ToList());
            // Lưu ConnectionId của client khi họ kết nối
            //_connectedClients[Context.ConnectionId] = Context.User.;

            //Gửi danh sách list user mới nhất xuống để font end update số người tham gia

            await base.OnConnectedAsync();
        }

        public async Task KeepAliveMethodOnServer()
        {
            await Clients.Caller.SendAsync("KeepAliveResponse", "Server received keep-alive at " + DateTime.Now);
        }

        //Test send all client update danh sách user mới
        public async Task send_list_user(List<User_hub> lu)
        {
            await Clients.All.SendAsync("list_user", lu);
        }
        public async Task user_on(string code,string Name)
        {
            await Clients.All.SendAsync("user_on", new  {telesale_code= code,telesale_name=Name });
        }
        public async Task user_off(string code, string Name)
        {
            await Clients.All.SendAsync("user_off", new { telesale_code = code, telesale_name = Name });
        }


        //Test send đến 1 client khi form là form chat group or chat 1 với 1
        public async Task SendMessageToClient(string connectionId, string message)
        {
            // Gửi tin nhắn đến client cụ thể bằng cách sử dụng ConnectionId
            await Clients.Client(connectionId).SendAsync("ReceiveMessage", message);
        }

        //Test send đến all client chat box
        public async Task SendMessageToAllClient(string message)
        {
            // Gửi tin nhắn đến client cụ thể bằng cách sử dụng ConnectionId
            await Clients.All.SendAsync("AllClientMessage", message);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            // Xóa ConnectionId của client khi họ ngắt kết nối
            //_connectedClients.Remove(Context.ConnectionId);
            User_hub? u_remove = (from p in _onlineUserService.GetOnlineUsers() where p.connection_id == Context.ConnectionId select p).FirstOrDefault();
            if (u_remove != null)
            {
                //lconnectedClients.Remove(u_remove);
                //await send_list_user(lconnectedClients);
            }
            _onlineUserService.RemoveUser(u_remove);
            await user_off(u_remove.employee_code, u_remove.display_name);
            await base.OnDisconnectedAsync(exception);
        }
    }
}
