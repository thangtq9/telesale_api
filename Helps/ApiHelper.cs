﻿using Hub_Pda.Api.Model;
using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace Hub_Pda.Api.Helps
{
    public class ApiHelper
    {
        public async Task<HttpResponseMessage> Call_API(string url, string router, Dictionary<string, string> header, Dictionary<string, string> parametter, HttpMethod method, object jsonbody, int timeout_seconds = 5)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                string fullUrl = "";
                if (parametter != null && parametter.Count > 0)
                {
                    fullUrl = "?" + string.Join("&", parametter.Select(x => $"{x.Key}={x.Value}"));
                }

                string jsonData = JsonConvert.SerializeObject(jsonbody);
                var client = new HttpClient();
                var request = new HttpRequestMessage(method, url + router + fullUrl);
                request.Headers.Add("accept", "*/*");

                if (header != null && header.Count > 0)
                {
                    foreach (var item in header)
                    {
                        request.Headers.Add(item.Key, item.Value);
                    }
                }

                var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                request.Content = content;
                client.Timeout = TimeSpan.FromSeconds(timeout_seconds);
                var response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                return response;
                //return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string ConvertUserPassToBasicAuthen(string username, string password)
        {
            string concatenatedCredentials = $"{username}:{password}";
            byte[] byteCredentials = Encoding.UTF8.GetBytes(concatenatedCredentials);
            string encodedCredentials = Convert.ToBase64String(byteCredentials);
            return $"Basic {encodedCredentials}";
        }
        public List<CusDummyModel> list_customer_dummy()
        {
            List<CusDummyModel> customer_dummy = new List<CusDummyModel>();
            customer_dummy.Add(new CusDummyModel("10010", "1117100019828"));
            customer_dummy.Add(new CusDummyModel("10011", "2217000032274"));
            customer_dummy.Add(new CusDummyModel("10029", "2223000044582"));
            return customer_dummy;
        }
        public object template_search_article(string prefix, string query, int store_no)
        {
            var j = new
            {
                _source = new[] { "retail_prices", "art-no", "art-name" },
                query = new
                {
                    @bool = new
                    {
                        must = new object[]
            {
                new {prefix = new Dictionary<string, object> { { "art-name", prefix } } },
                 new
                {
                   match = new Dictionary<string, object>
                    {
                        { "art-name", new { query = query, @operator = "and" } }
                    }
                },
                new
                {
                    match = new Dictionary<string, object>
                    {
                        { "retail_prices.store_id", store_no }
                    }
                }
            },
                        must_not = new[]
            {
                new { match = new Dictionary<string, object> { { "art-name", "QT-" } } }
            },
                        filter = new[]
            {
                new
                {
                    range = new Dictionary<string, object>
                    {
                        { "retail_prices.status", new { lt = 2 } }
                    }
                }
            }
                    }
                },
                sort = new[]
    {
        new
        {
            _score = new
            {
                order = "desc"
            }
        }
    },
                from = 0,
                size = 10
            };

            return j;
        }

        public object template_search_article_contain(string prefix, string query, int store_no)
        {
            var _wildcard = new object[1];
            string[] key_word = query.Split(' ');
            var j = new
            {
                _source = new[] { "retail_prices", "art-no", "art-name" },
                query = new
                {
                    @bool = new
                    {
                        must = new object[]
            {
                //new {prefix = new Dictionary<string, object> { { "art-name", prefix } } },
                //new
                //{
                //    wildcard = new Dictionary<string, object>
                //    {
                //        { "art-name", "*" + prefix }
                //    }
                //},
                 new
                {
                    wildcard = new Dictionary<string, object>
                    {
                        { "art-name", "*" + (key_word.Count()>=1?key_word[0]: prefix) +"*" }
                    }
                },
                  new
                {
                    wildcard = new Dictionary<string, object>
                    {
                        { "art-name", "*" + (key_word.Count()>=2?key_word[1]: prefix) +"*" }
                    }
                },
                    new
                {
                    wildcard = new Dictionary<string, object>
                    {
                        { "art-name", "*" + (key_word.Count()>=3?key_word[2]: prefix) +"*" }
                    }
                },
                        new
                {
                    wildcard = new Dictionary<string, object>
                    {
                        { "art-name", "*" + (key_word.Count()>=4?key_word[3]: prefix) +"*" }
                    }
                },
                            new
                {
                    wildcard = new Dictionary<string, object>
                    {
                        { "art-name", "*" + (key_word.Count()>=5?key_word[4]: prefix) +"*" }
                    }
                },
                new
                {
                    match = new Dictionary<string, object>
                    {
                        { "retail_prices.store_id", store_no }
                    }
                }
            },
                        must_not = new[]
            {
                new { match = new Dictionary<string, object> { { "art-name", "QT-" } } }
            },
                        filter = new[]
            {
                new
                {
                    range = new Dictionary<string, object>
                    {
                        { "retail_prices.status", new { lt = 2 } }
                    }
                }
            }
                    }
                },
                sort = new[]
    {
        new
        {
            _score = new
            {
                order = "desc"
            }
        }
    },
                from = 0,
                size = 10
            };

            return j;
        }


    }


}
