﻿using Hub_Pda.Api.Entities;
using Hub_Pda.Api.Helps;
using Hub_Pda.Api.Model;
using Hub_Pda.Api.Model.Zalo;
using Hub_Pda.Api.Repositories;
using Hub_Pda.Api.Services;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using ServiceStack.Redis;

namespace Hub_Pda.Api.BServices
{
    public class MessProcessingService : BackgroundService
    {
        private readonly OnlineUserService _onlineUserService;
        protected readonly IConfiguration Configuration;
        private readonly IHubContext<AuctionHub> _hubContext;
        private readonly IRedisClientsManager _redisManager = null;
        private readonly IRedisClient _redisclient = null;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IServiceScopeFactory _serviceScopeFactory1;
        //private readonly IArticleService aService;
        //private readonly ITeleSaleService tleService;
        private readonly MessageQueueService mpService;
        public MessProcessingService(IServiceScopeFactory serviceScopeFactory, IServiceScopeFactory serviceScopeFactory1, IHubContext<AuctionHub> hubContext, IConfiguration configuration, OnlineUserService onlineUserService, MessageQueueService _mpService)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _serviceScopeFactory1 = serviceScopeFactory1;
            this._hubContext = hubContext;
            //this.aService = articleService;
            //this.tleService = _tleService;
            _hubContext = hubContext;
            Configuration = configuration;
            _onlineUserService = onlineUserService;
            if (_redisManager == null)
            {
                try
                {
                    RedisPoolConfig _config = new RedisPoolConfig();
                    _config.MaxPoolSize = 100;
                    if (string.IsNullOrEmpty(Configuration["AppSettings:RedisPassword"]))
                        _redisManager = new RedisManagerPool(Configuration["AppSettings:RedisIP"], _config);
                    else
                    {
                        var config = new RedisEndpoint
                        {
                            Host = Configuration["AppSettings:RedisIP"],
                            Port = 6379,
                            Password = Configuration["AppSettings:RedisPassword"],
                            ConnectTimeout = 2000,
                            ReceiveTimeout = 2000,
                            SendTimeout = 2000,
                            RetryTimeout = 500,
                        };
                        _redisManager = new RedisManagerPool(config.ToString(), _config);
                    }
                    _redisclient = _redisManager.GetClient();
                }
                catch (Exception)
                {
                }
            }
            this.mpService = _mpService;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (mpService.TryDequeueMess(out web_hub_mes_model? wem))
                {
                    #region Log local
                    string path_log = Directory.GetCurrentDirectory() + "\\Logs";
                    if (!Directory.Exists(path_log))
                        Directory.CreateDirectory(path_log);

                    string filename = "queue_" + DateTime.Now.ToString("yyyyMMdd");
                    string Log = System.Environment.NewLine + JsonConvert.SerializeObject(wem);
                    FileStream _stream = null;
                    StreamWriter sw = null;
                    try
                    {
                        string Path = path_log + "/" + filename + ".log";

                        if (!System.IO.File.Exists(Path))
                            _stream = System.IO.File.Create(Path);
                        else
                        {
                            FileInfo file = new FileInfo(Path);
                            if (file.Length > 20000000)
                            {
                                file.Delete();
                                _stream = System.IO.File.Create(Path);
                            }
                            else
                                _stream = new FileStream(Path, FileMode.Append);
                        }
                        sw = new StreamWriter(_stream);
                        sw.Write("***" + DateTime.Now.ToString() + Environment.NewLine + Log + Environment.NewLine);
                        sw.Flush();
                    }
                    catch (Exception ex)
                    {
                    }
                    if (sw != null)
                    {
                        sw.Close();
                        sw.Dispose();
                    }
                    if (_stream != null)
                    {
                        _stream.Close();
                        _stream.Dispose();
                    }
                    #endregion
                    oa_list_by_store obs = new oa_list_by_store();
                    if (_redisclient != null)
                        obs = _redisclient.Get<oa_list_by_store>("ZALO_OA_" + wem.recipient.id);
                    if (obs == null)
                    {
                        using (var scope = _serviceScopeFactory.CreateScope())
                        {
                            var _aService = scope.ServiceProvider.GetRequiredService<IArticleService>();
                            obs = await _aService.GetStoreOAInfoById(wem.recipient.id);
                            if (_redisclient != null)
                                _redisclient.Add("ZALO_OA_" + wem.recipient.id, obs, DateTime.Now.AddHours(12));
                        }
                    }

                    if (wem != null && wem.event_name.Equals("user_send_text") && obs != null)
                    {
                        string tele_sale_code_discuss = "";
                        customer_zalo cz = new customer_zalo();
                        try
                        {
                            using (var scope = _serviceScopeFactory1.CreateScope())
                            {
                                var _tleService = scope.ServiceProvider.GetRequiredService<ITeleSaleService>();

                                if (_redisclient != null)
                                    cz = _redisclient.Get<customer_zalo>("ZALO_CUS_" + wem.sender.id + "_" + obs.oa_id);
                                if (cz == null)
                                {
                                    cz = await _tleService.GetCusByZaloId(wem.sender.id, obs.oa_id, obs.store_no);
                                    if (_redisclient != null)
                                        _redisclient.Add("ZALO_CUS_" + wem.sender.id + "_" + obs.oa_id, cz, DateTime.Now.AddMinutes(30));
                                }
                                if (cz != null && !string.IsNullOrEmpty(cz.telesale_code) && cz.telesale_code != "EMPTY")
                                    tele_sale_code_discuss = cz.telesale_code!;

                                if (cz != null)
                                {
                                    messages me = await _tleService.AddMess(wem.sender.id,wem.message.msg_id, cz.telesale_code, wem.message.text, (int)Enum_User_Input.CUSTOMER);
                                }
                                else
                                    await _tleService.AddMess(wem.sender.id, wem.message.msg_id, null, wem.message.text, (int)Enum_User_Input.CUSTOMER);
                            }

                        }
                        catch (Exception ex)
                        {
                            var e = ex;
                        }
                        try
                        {
                            string telesale_code = "";
                            if (cz != null && !string.IsNullOrEmpty(cz.telesale_code))
                                telesale_code = cz.telesale_code;
                            List<User_hub> users = _onlineUserService.GetOnlineUsers().ToList();
                            if (users != null)
                            {
                                if (!string.IsNullOrEmpty(tele_sale_code_discuss))
                                {
                                    User_hub? uh = (from p in users where p.employee_code == tele_sale_code_discuss select p).FirstOrDefault();
                                    if (uh != null)
                                    {
                                        await _hubContext.Clients.Client(uh.connection_id).SendAsync("new_mes", new { cus_zalo_id = wem.sender.id, cus_name = string.IsNullOrEmpty(cz.cus_name) ? "KH mới" : cz.cus_name, text = wem.message.text, modify_date = DateTime.Now, mes_not_read = 1, store_no = obs.store_no, telesale_code = telesale_code });
                                        //_onlineUserService.AddCustomerToUser(wem.sender.id, uh.employee_code);
                                    }
                                }
                                else
                                {
                                    await _hubContext.Clients.All.SendAsync("new_mes", new { cus_zalo_id = wem.sender.id, cus_name = string.IsNullOrEmpty(cz.cus_name) ? "KH mới" : cz.cus_name, text = wem.message.text, modify_date = DateTime.Now, mes_not_read = 1, store_no = obs.store_no, telesale_code = telesale_code });
                                }
                            }
                        }
                        catch (Exception ex)
                        { 
                        }
                    }
                }
                else
                {
                    await Task.Delay(500, stoppingToken);
                }
            }
        }
    }
}
