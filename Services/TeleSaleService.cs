﻿using DocumentFormat.OpenXml.InkML;
using DocumentFormat.OpenXml.Wordprocessing;
using Hub_Pda.Api.Context;
using Hub_Pda.Api.Entities;
using Hub_Pda.Api.Helps;
using Hub_Pda.Api.Model;
using Hub_Pda.Api.Repositories;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Security.Cryptography;

namespace Hub_Pda.Api.Services
{
    public class TeleSaleService : ITeleSaleService
    {
        private readonly TeleSaleContextClass _dbContext;
        private readonly OnlineUserService _onlineUserService;
        /// <summary>
        /// Init
        /// </summary>
        public TeleSaleService(TeleSaleContextClass dbContext, OnlineUserService onlineUserService)
        {
            _dbContext = dbContext;
            _onlineUserService = onlineUserService;
        }
        public async Task<customer_zalo> GetCusByZaloId(string customer_zalo_id, string? oa_id, string? store_no)
        {
            var cus = await _dbContext.customer_zalo.FirstOrDefaultAsync(p => p.cus_zalo_id == customer_zalo_id);
            if (cus == null)
            {
                customer_zalo cz = new customer_zalo();
                cz.cus_zalo_id = customer_zalo_id;
                cz.oa_id = oa_id;
                cz.create_date = DateTime.Now;
                cz.status = Enum_Cus_Status.New;
                cz.store_no = store_no;
                _dbContext.customer_zalo.Add(cz);
                _dbContext.SaveChanges();
                return cz;
            }
            if (cus != null && oa_id != null && oa_id != cus.oa_id)
            {

                cus.oa_id = oa_id;
                cus.modify_date = DateTime.Now;
                if (cus.status == Enum_Cus_Status.Done)
                    cus.status = Enum_Cus_Status.New;
                _dbContext.SaveChanges();
                return cus;
            }
            else
                return cus;
        }


        public async Task<List<customer_zalo>> GetCusDiscussNewByTeleSale(string telesale_code)
        {
            //List<customer_zalo> lmm = await _dbContext.customer_zalo.Where(x => (x.status == Enum_Cus_Status.New || x.status == Enum_Cus_Status.Inprogress))
            //             .ToListAsync();
            List<customer_zalo> lmm = await _dbContext.customer_zalo.OrderBy(x=>x.status).ToListAsync();
            return lmm;
        }

        public async Task<List<messages>> GetMesByCusZaloId(string telesale_code, string customer_zalo_id, int pageNumber = 1)
        {
            List<messages> lmm = await _dbContext.messages.Where(e => e.cus_zalo_id == customer_zalo_id)
                        .OrderByDescending(e => e.message_id)
                         .Skip((pageNumber - 1) * 20)
                         .Take(20)
                         .ToListAsync();
            return lmm;
        }


        public async Task<int> UpdateCusInfo(customer_zalo cz)
        {
            var cus = await _dbContext.customer_zalo.FirstOrDefaultAsync(p => (p.cus_zalo_id == cz.cus_zalo_id || (!string.IsNullOrEmpty(p.ecp_customer_no) && !string.IsNullOrEmpty(cz.ecp_customer_no) && p.ecp_customer_no == cz.ecp_customer_no)));
            if (cus != null)
            {
                cus.status = (cz.status != "EMPTY" ? cz.status : cus.status);
                cus.cus_name = (cz.cus_name != "EMPTY" ? cz.cus_name : cus.cus_name);
                cus.cus_mobile = (cz.cus_mobile != "EMPTY" ? cz.cus_mobile : cus.cus_mobile);
                cus.cus_deli_address = (cz.cus_deli_address != "EMPTY" ? cz.cus_deli_address : cus.cus_deli_address);
                cus.cus_type = cz.cus_type != "EMPTY" ? cz.cus_type : cus.cus_type;
                cus.ecp_customer_no = cz.ecp_customer_no != "EMPTY" ? cz.ecp_customer_no : cus.ecp_customer_no;
                cus.store_no = cz.store_no != "EMPTY" ? cz.store_no : cus.store_no;
                cus.mes_not_read = cz.mes_not_read != -1 ? cz.mes_not_read : cus.mes_not_read;
                cus.modify_date = DateTime.Now;
                cus.oa_id = cz.oa_id != "EMPTY" ? cz.oa_id : cus.oa_id;
                cus.telesale_code = cz.telesale_code != "EMPTY" ? cz.telesale_code : cus.telesale_code;
                return _dbContext.SaveChanges();
            }
            else
                return 0;
        }
        public async Task<int> AddCusInfo(customer_zalo cz)
        {
            var cus = await _dbContext.customer_zalo.FirstOrDefaultAsync(p => p.cus_zalo_id == cz.cus_zalo_id);
            if (cus == null)
            {
                _dbContext.customer_zalo.Add(cz);
                return _dbContext.SaveChanges();
            }
            return 0;
        }

        public async Task<messages> AddMess(string cus_zalo_id,string msg_id, string telesale_code, string text, int user_chat)
        {
            messages m = new messages();
            m.text = text;
            m.time = DateTime.Now;
            m.telesale_code = telesale_code;
            m.cus_zalo_id = cus_zalo_id;
            m.msg_id = msg_id;
            m.user_chat = user_chat;
            _dbContext.messages.Add(m);

            if (user_chat == (int)Enum_User_Input.TELESALE)
            {
                var cus = await _dbContext.customer_zalo.FirstOrDefaultAsync(p => p.cus_zalo_id == cus_zalo_id);
                if (cus != null)
                {
                    cus.modify_date = DateTime.Now;
                    cus.mes_not_read = 0;
                    cus.text = text;
                }
            }
            if (user_chat == (int)Enum_User_Input.CUSTOMER)
            {
                string _cus_zalo_id_active = _onlineUserService.GetCustomer_Select(telesale_code);
                var cus = await _dbContext.customer_zalo.FirstOrDefaultAsync(p => p.cus_zalo_id == cus_zalo_id);
                if (cus != null)
                {
                    cus.modify_date = DateTime.Now;
                    if (!string.IsNullOrEmpty(_cus_zalo_id_active) && _cus_zalo_id_active == cus_zalo_id)
                        cus.mes_not_read = 0;
                    else
                        cus.mes_not_read = cus.mes_not_read + 1;
                    cus.text = text;
                }
            }
            await _dbContext.SaveChangesAsync();
            return m;
        }



        public async Task<List<orders>> get_order_by_cus_no(string customer_no)
        {
            List<orders> lmm = await _dbContext.orders.Where(x => x.ecp_customer_no == customer_no)
                        .OrderByDescending(e => e.order_id)
                         .Take(10)
                         .ToListAsync();
            return lmm;
        }
        public async Task<List<orders>> get_order_by_mobile(string mobile)
        {
            List<orders> lmm = await _dbContext.orders.Where(x => x.cus_phone == mobile)
                        .OrderByDescending(e => e.order_id)
                         .Take(10)
                         .ToListAsync();
            return lmm;
        }

        public async Task<orders?> get_order_by_code(string order_code)
        {
            orders? lmm = await _dbContext.orders.Where(x => x.order_code == order_code).FirstOrDefaultAsync();

            return lmm;
        }

        public async Task<List<order_details>> get_order_detail(long order_id)
        {
            List<order_details> lmm = await _dbContext.order_details.Where(x => x.order_id == order_id)
                         .ToListAsync();
            return lmm;
        }
        public async Task<int> get_order_seq()
        {
            order_seq os = new order_seq();
            os = await _dbContext.order_seq.FirstOrDefaultAsync();
            return os.o_seq;
        }
        public async Task<List<orders>> get_order_list()
        {
            List<orders> odl = await _dbContext.orders.ToListAsync();
            return odl;
        }


        public DataSet get_order_chanel_zns(string order_no)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@order_no", order_no));
            DataSet dt = new DataSet();
            dt = _dbContext.ExecuteDataSet("[dbo].[Zalo.GetOrderDetail]", 120, parameter.ToArray());
            return dt;
        }

    }
}
