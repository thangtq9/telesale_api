﻿using Hub_Pda.Api.Context;
using Hub_Pda.Api.Entities;
using Hub_Pda.Api.Repositories;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace Hub_Pda.Api.Services
{
    public class ArticleService : IArticleService
    {
        private readonly DbContextClass _dbContext;
        /// <summary>
        /// Init
        /// </summary>
        public ArticleService(DbContextClass dbContext)
        {
            _dbContext = dbContext;
        }
        /// <summary>
        /// Get Productlist by List Article, store no.Requid
        /// </summary>
        //public async Task<List<Article>> GetProductListAsync(string ListArticleNo, string store_no)
        //{
        //    var parameter = new List<SqlParameter>();
        //    parameter.Add(new SqlParameter("@ListArticleNo", ListArticleNo));
        //    parameter.Add(new SqlParameter("@store_no", store_no));

        //    var productDetails = await Task.Run(() => _dbContext.Article
        //                   .FromSqlRaw(@"exec GetArticleByArticleNo @ListArticleNo @store_no", parameter.ToArray()).ToListAsync());
        //    return productDetails;
        //}
        //public DataTable GetArticleList(string listarticle, string list_own_brand, string list_supplier_brand, string list_supplier)
        //{
        //    var parameter = new List<SqlParameter>();
        //    parameter.Add(new SqlParameter("@listarticle", listarticle));
        //    parameter.Add(new SqlParameter("@list_own_brand", list_own_brand));
        //    parameter.Add(new SqlParameter("@list_supplier_brand", list_supplier_brand));
        //    parameter.Add(new SqlParameter("@list_supplier", list_supplier));
        //    DataTable dt = new DataTable();
        //    dt = _dbContext.ExecuteDataTable("GetArticleList_LG", 120, parameter.ToArray());
        //    return dt;
        //}
        public async Task<oa_list_by_store> GetStoreOAInfo(string store_no)
        {
            var oa_info = _dbContext.oa_list_by_store
    .Where(x => x.store_no == store_no).FirstOrDefault();
            return oa_info;
        }
        public async Task<int> UpdateStoreOAInfo(oa_list_by_store ols, string new_refresh_token)
        {
            ols.refresh_token = new_refresh_token;
            ols.modifi_date = DateTime.Now;
            return _dbContext.SaveChanges();
        }
        public async Task<oa_list_by_store> GetStoreOAInfoById(string oa_id)
        {
            var oa_info = _dbContext.oa_list_by_store
    .Where(x => x.oa_id == oa_id).FirstOrDefault();
            return oa_info;
        }
    }
}
