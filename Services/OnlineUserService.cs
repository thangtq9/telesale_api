﻿using Hub_Pda.Api.Model;
using ServiceStack;
using System.Collections.Generic;

namespace Hub_Pda.Api.Services
{
    public class OnlineUserService
    {
        private List<User_hub> _onlineUsers = new List<User_hub>();
        private Dictionary<string, List<string>> User_Customer = new Dictionary<string, List<string>>();

        private Dictionary<string, string> Customer_Select = new Dictionary<string, string>();


        public void AddCustomer_Select(string telesale_code, string cus_zalo_id)
        {
            string _cus_zalo_id = "";
            if (Customer_Select.TryGetValue(telesale_code, out _cus_zalo_id))
            {
                if (!string.IsNullOrEmpty(_cus_zalo_id))
                    Customer_Select.Remove(telesale_code);
                Customer_Select[telesale_code] = cus_zalo_id;
            }
            else
            {
                Customer_Select[telesale_code] = cus_zalo_id;
            }
        }
        public string GetCustomer_Select(string telesale_code)
        {
            string _cus_zalo_id = "";
            if (Customer_Select.TryGetValue(telesale_code, out _cus_zalo_id))
            {
            }
            return _cus_zalo_id;
        }


        public void AddUser(User_hub uhm)
        {
            _onlineUsers.Add(uhm);
        }

        public void RemoveUser(User_hub uhm)
        {
            _onlineUsers.RemoveAll(p => p.employee_code == uhm.employee_code);
            try
            {
                User_Customer.Remove(uhm.employee_code);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private readonly object lockObject = new object();
        public void AddCustomerToUser(string zalo_customer_id, string employee_code, string employee_code_old = "")
        {
            if (!string.IsNullOrEmpty(employee_code_old))
            {
                List<string>? l_user = new List<string>();
                if (User_Customer.TryGetValue(employee_code, out l_user))
                {
                    if (l_user != null && l_user.Count > 0)
                    {
                        l_user.Remove(employee_code);
                        User_Customer[employee_code] = l_user;
                    }
                }
                l_user = new List<string>();
                if (User_Customer.TryGetValue(employee_code_old, out l_user))
                {
                    if (l_user != null && l_user.Count > 0)
                    {
                        l_user.Add(employee_code_old);
                        User_Customer[employee_code_old] = l_user;
                    }
                    else
                    {
                        l_user = new List<string>();
                        l_user.Add(employee_code_old);
                        User_Customer[employee_code_old] = l_user;
                    }    
                }
            }
            else
            {
                lock (lockObject)
                {
                    bool found = false;
                    foreach (KeyValuePair<string, List<string>> entry in User_Customer)
                    {
                        if (entry.Value.Contains(zalo_customer_id))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        List<string>? l_user = new List<string>();
                        if (User_Customer.TryGetValue(employee_code, out l_user))
                        {
                            if (l_user != null && l_user.Count > 0)
                            {
                                if (l_user.Exists(x => x == zalo_customer_id) == false)
                                {
                                    l_user.Add(zalo_customer_id);
                                    User_Customer[employee_code] = l_user;
                                }
                            }
                        }
                        else
                        {
                            l_user = new List<string>(); l_user.Add(zalo_customer_id);
                            User_Customer.Add(employee_code, l_user);
                        }
                    }
                }
            }

            User_hub uh = (from p in _onlineUsers where p.employee_code == employee_code select p).FirstNonDefault();
            if (uh != null)
            {
                _onlineUsers.Find(x => x.employee_code == employee_code).count_customer = uh.count_customer + 1;
            }
        }

        public List<string>? GetCustomerByUser(string telsale_code)
        {
            List<string>? l_user = new List<string>();
            if (User_Customer.TryGetValue(telsale_code, out l_user))
            {

            }
            return l_user;

        }

        public string? GetConnectIdUserByCustomer(string zalo_customer_id)
        {
            // Duyệt qua từ điển để tìm giá trị
            if (User_Customer != null)
            {
                foreach (var pair in User_Customer)
                {
                    if (pair.Value.Contains(zalo_customer_id))
                    {
                        foreach (var u_online in _onlineUsers)
                        {
                            if (pair.Key == u_online.employee_code)
                            {
                                return u_online.connection_id;
                            }
                        }
                    }
                }
            }
            return null;
        }

        public IEnumerable<User_hub> GetOnlineUsers()
        {
            return _onlineUsers;
        }
        public Dictionary<string, List<string>> GetUsersCustomerAll()
        {
            return User_Customer;
        }
        public Dictionary<string, string> GetCustomerSelectAll()
        {
            return Customer_Select;
        }
    }
}
