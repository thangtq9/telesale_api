﻿using Hub_Pda.Api.Context;
using Hub_Pda.Api.Model;
using Hub_Pda.Api.Repositories;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Reflection.Metadata;

namespace Hub_Pda.Api.Services
{
    public class PdaService : IPdaService
    {
        private readonly DbContextClass _dbContext;
        public PdaService(DbContextClass dbContext) { 
            this._dbContext = dbContext;
        }

        public DataTable GetListStore(int? store_no, int type)
        {
            var parameter = new List<SqlParameter>();
            if(store_no == 0)
            {
                store_no = null;
            }
            parameter.Add(new SqlParameter("@StoreNo", store_no));
            parameter.Add(new SqlParameter("@Type", 0));
            DataTable dt = new DataTable();
            dt = _dbContext.ExecuteDataTable("[dbo].[Mobile.Hub.GetStoreList]", 120, parameter.ToArray());
            return dt;
        }

        public DataTable GetPDADeviceInfo(int? pda_id)
        {
            throw new NotImplementedException();
        }

        public DataTable GetPDAList(int? store_no, int type)
        {
            var parameter = new List<SqlParameter>();
            DataTable dt = new DataTable();
            parameter.Add(new SqlParameter("@StoreNo", store_no));
            parameter.Add(new SqlParameter("@Type", 1));
            dt = _dbContext.ExecuteDataTable("[dbo].[Mobile.Hub.GetStoreList]", 120, parameter.ToArray());
            return dt;
        }
    }
}
