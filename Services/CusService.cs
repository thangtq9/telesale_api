﻿using Hub_Pda.Api.Context;
using Hub_Pda.Api.Model;
using Hub_Pda.Api.Repositories;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Reflection.Metadata;

namespace Hub_Pda.Api.Services
{
    public class CusService :ICusService
    {
        private readonly DbContextClass _dbContext;
        public CusService(DbContextClass dbContext)
        {
            this._dbContext = dbContext;
        }
        public DataTable GetCusInfo(string? cus_phone_num)
        {
            var parameter = new List<SqlParameter>();
            if(string.IsNullOrEmpty(cus_phone_num))
            {
                cus_phone_num = null;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
            }
            parameter.Add(new SqlParameter("@Phone", cus_phone_num));
            parameter.Add(new SqlParameter("@Type", "Dummy"));
            DataTable dt= new DataTable();
            dt = _dbContext.ExecuteDataTable("[dbo].[Mobile.Hub.GetStoreList]", 120, parameter.ToArray());
            return dt;
        }

        public DataTable GetProList(string store_name, string pro_name)
        {
            throw new NotImplementedException();
        }
    }
}
