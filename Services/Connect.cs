﻿using System.Net.Http;
using System.Threading.Tasks;
namespace Hub_Pda.Api.Services
{
    public class Connect
    {
        private static readonly HttpClient Client = new HttpClient();
        public static async Task Main(string[] args)
        {
            await CallApi();
        }
        public static async Task CallApi()
        {
            try
            {
                HttpResponseMessage response = await Client.GetAsync("https://172.26.16.107:5000/api/customer/fintByCriteria");
                if (response.IsSuccessStatusCode)
                {
                    string responseContent = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(responseContent);
                }
                else
                {
                    Console.WriteLine("Request failed with status code: " + response.StatusCode);
                }
            }
            catch (Exception ex) {
            Console.WriteLine(ex.Message);}
        }
    }
}
