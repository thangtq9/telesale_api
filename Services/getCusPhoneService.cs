﻿using Hub_Pda.Api.Context;
using Hub_Pda.Api.Entities;
using Hub_Pda.Api.Model;
using Hub_Pda.Api.Repositories;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Reflection.Metadata;
namespace Hub_Pda.Api.Services
{
    public class getCusPhoneService : IgetCusPhoneService
    {
        private readonly CustomerDbContext _dbContext;
        public getCusPhoneService(CustomerDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<customer_card> GetCusAdd(string mobile_no)
        {
            if (mobile_no.StartsWith("0"))
                mobile_no = mobile_no.Substring(1, mobile_no.Length - 1);
            if (mobile_no.StartsWith("84"))
                mobile_no = mobile_no.Substring(1, mobile_no.Length - 2);
            var cusAdd = _dbContext.customer_card
            .Where(x => x.mobile_no.Contains(mobile_no)).ToList()
            .FirstOrDefault();

            return cusAdd;

        }
        public async Task<customer_info> GetCusType(string customer_id)
        {
            var custype = _dbContext.customer_info
           .Where(x => x.customer_id == customer_id).ToList()
           .FirstOrDefault();
            return custype;
        }
    }

}

