﻿using Hub_Pda.Api.Model;
using Hub_Pda.Api.Model.Zalo;
using System.Collections.Concurrent;

namespace Hub_Pda.Api.Services
{
    public class MessageQueueService
    {
        public ConcurrentQueue<web_hub_mes_model> MesssQueue { get; } = new ConcurrentQueue<web_hub_mes_model>();

        public void EnqueueMess(web_hub_mes_model order)
        {
            MesssQueue.Enqueue(order);
        }

        public bool TryDequeueMess(out web_hub_mes_model? order)
        {
            return MesssQueue.TryDequeue(out order);
        }
    }
}
