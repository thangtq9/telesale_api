﻿using Hub_Pda.Api.Entities;
using System.Data;

namespace Hub_Pda.Api.Repositories
{
    public interface ITeleSaleService
    {
        public Task<customer_zalo> GetCusByZaloId(string customer_zalo_id, string? oa_id,string? store_no);

        public Task<List<customer_zalo>> GetCusDiscussNewByTeleSale(string telesale_code);

        public Task<List<messages>> GetMesByCusZaloId(string telesale_code, string customer_zalo_id, int page_index);

        public Task<int> AddCusInfo(customer_zalo cz);
        public Task<int> UpdateCusInfo(customer_zalo cz);

        public Task<messages> AddMess(string cus_zalo_id,string msg_id, string? telesale_code, string text, int user_chat);
        public Task<List<orders>> get_order_by_cus_no(string customer_no);
        public Task<List<orders>> get_order_by_mobile(string mobile);
        public Task<orders> get_order_by_code(string order_code);
        public Task<List<order_details>> get_order_detail(long order_id);
        public Task<List<orders>> get_order_list();
        public Task<int> get_order_seq();

        public DataSet get_order_chanel_zns(string order_no);
    }
}
