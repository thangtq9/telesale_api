﻿using Hub_Pda.Api.Entities;
using System.Data;

namespace Hub_Pda.Api.Repositories
{
    public interface IArticleService
    {
        /// <summary>
        /// Get Productlist by List Article, store no.Requid
        /// </summary>
        //public DataTable GetArticleList(string listarticle, string list_own_brand, string list_supplier_brand, string list_supplier);
        public Task<oa_list_by_store> GetStoreOAInfo(string store_no);
        public Task<int> UpdateStoreOAInfo(oa_list_by_store ols, string new_refresh_token);
        public Task<oa_list_by_store> GetStoreOAInfoById(string oa_id);
    }
}
