﻿using Hub_Pda.Api.Entities;
using System.Data;

namespace Hub_Pda.Api.Repositories
{
    public interface IgetCusPhoneService
    {
        public Task<customer_card> GetCusAdd(string mobile_no);
        public Task<customer_info> GetCusType(string customer_id);
    }
}
