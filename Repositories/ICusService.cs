﻿using Hub_Pda.Api.Model;
using System.Data;

namespace Hub_Pda.Api.Repositories
{
    public interface ICusService
    {
        public DataTable GetCusInfo(string? cus_phone_num);
        public DataTable GetProList(string store_name, string pro_name);

    }
}
