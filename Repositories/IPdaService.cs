﻿using Hub_Pda.Api.Model;
using System.Data;

namespace Hub_Pda.Api.Repositories
{
    public interface IPdaService
    {
        public DataTable GetListStore(int? store_no, int type);
        public DataTable GetPDAList(int? store_no, int type);
        public DataTable GetPDADeviceInfo(int? pda_id);
    }
}

