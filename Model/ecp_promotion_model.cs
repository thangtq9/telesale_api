﻿namespace TeleSale.Api.Model
{

    // Root myDeserializedClass = JsonConvert.DeserializeObject<ecp_promotion_model>(myJsonResponse);
    public class DatumPromotion
    {
        public string dnr_promotion_year { get; set; }
        public string campaign_name { get; set; }
        public string dnr_promotion_id { get; set; }
        public string dnr_promotion_name { get; set; }
        public int dnr_discount_code { get; set; }
        public string dnr_start_date { get; set; }
        public string dnr_end_date { get; set; }
        public int no_of_article { get; set; }
        public int no_of_store { get; set; }
        public string dnr_promotion_status { get; set; }
    }

    public class ecp_promotion_model
    {
        public string? result_code { get; set; }
        public string? result_message { get; set; }
        public string? api_type { get; set; }
        public string? trans_id { get; set; }
        public int? page_size { get; set; }
        public int? page_no { get; set; }
        public int? data_total { get; set; }
        public List<DatumPromotion>? data { get; set; }
    }



}
