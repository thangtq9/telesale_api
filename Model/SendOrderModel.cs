﻿namespace Hub_Pda.Api.Model
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class OrderItem
    {
        public int item_seq { get; set; }
        public string article_code { get; set; }
        public float order_qty { get; set; }
        public object remark { get; set; }
        public string price_code { get; set; }
        public object discount_code { get; set; }
        public object discount_event { get; set; }
        public double fulfillment_price { get; set; }
        public int vat_rate { get; set; }
        public int vat_code { get; set; }
        public double unit_price_ex_vat { get; set; }
        public int item_discount_ex_vat { get; set; }
        public object qty_discount { get; set; }
        public double total_price_ex_vat { get; set; }
        public double total_price_in_vat { get; set; }
        public double vat_after_disc { get; set; }
    }

    public class OrderItemDiscount
    {
        public object item_seq { get; set; }
        public object qty_discount { get; set; }
        public object discount_ex_vat { get; set; }
        public object reward_qty { get; set; }
        public object discount_type { get; set; }
        public object discount_code { get; set; }
        public object discount_rate { get; set; }
        public object discount_event { get; set; }
    }

    public class PaymentList
    {
        public string payment_code { get; set; }
        public string payment_name { get; set; }
        public object payment_no { get; set; }
        public string prepaid_flag { get; set; }
        public decimal amount { get; set; }
    }

    public class SendOrderModel
    {
        public string order_id { get; set; }
        public string po_id { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string sale_channel { get; set; }
        public string order_status { get; set; }
        public string customer_no { get; set; }
        public string customer_name { get; set; }
        public string vat_indicator { get; set; }
        public string representative { get; set; }
        public int sale_order_source { get; set; }
        public string store_code { get; set; }
        public string payment_type { get; set; }
        public string fulfillment_type { get; set; }
        public object invoice_address_id { get; set; }
        public string fiscal_no { get; set; }
        public string receiver_name { get; set; }
        public string receiver_phone { get; set; }
        public object delivery_office_phone { get; set; }
        public object delivery_email { get; set; }
        public object delivery_fax { get; set; }
        public string delivery_date { get; set; }
        public string invoice_address { get; set; }
        public string delivery_address { get; set; }
        public string delivery_time_from { get; set; }
        public string delivery_time_to { get; set; }
        public object send_order_mail_flag { get; set; }
        public object send_order_email { get; set; }
        public object internal_comment { get; set; }
        public object picking_comment { get; set; }
        public object Invoice_comment { get; set; }
        public object delivery_comment { get; set; }
        public double total_price_ex_vat { get; set; }
        public int total_discount_ex_vat { get; set; }
        public double total_vat { get; set; }
        public List<PaymentList> payment_list { get; set; }
        public List<OrderItem> order_items { get; set; }
        public List<OrderItemDiscount> order_item_discount { get; set; }
        
    }
    
}
