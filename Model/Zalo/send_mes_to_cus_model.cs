﻿namespace Hub_Pda.Api.Model.Zalo
{
    public class send_mes_to_cus_model
    {
        public string store_code { set; get; } 
        public string cus_zalo_id { set; get; } 
        public string text { set; get; } 
        public string telesale_code { set; get; }
    }
}
