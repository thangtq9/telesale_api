﻿namespace Hub_Pda.Api.Model.Zalo
{
    public class zalo_response_sms_model
    {
        public int error { set; get; } = 0;
        public string message { get; set; } = "";
    }
}
