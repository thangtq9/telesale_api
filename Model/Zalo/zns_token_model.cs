﻿namespace Hub_Pda.Api.Model.Zalo
{
    public class zns_token_model
    {
        public string access_token { get; set; } = "";
        public string refresh_token { get; set; } = "";
        public string expires_in { get; set; } = "";
    }
}
