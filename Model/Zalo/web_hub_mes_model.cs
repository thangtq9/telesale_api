﻿namespace Hub_Pda.Api.Model.Zalo
{
    public class Message
    {
        public string text { get; set; }
        public string msg_id { get; set; }
    }

    public class Recipient
    {
        public string id { get; set; }
    }

    public class web_hub_mes_model
    {
        public string event_name { get; set; }
        public string app_id { get; set; }
        public Sender sender { get; set; }
        public Recipient recipient { get; set; }
        public Message message { get; set; }
        public string timestamp { get; set; }
        public string user_id_by_app { get; set; }
    }

    public class Sender
    {
        public string id { get; set; }
    }
}
