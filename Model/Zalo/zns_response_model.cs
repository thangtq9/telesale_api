﻿namespace Hub_Pda.Api.Model.Zalo
{
    public class zns_response_model
    {
        public string v_tran_id { get; set; } = "";
        public string status_code { get; set; }
        public string msg { get; set; } = "";
        public int error { get; set; } = 0;
    }
}
