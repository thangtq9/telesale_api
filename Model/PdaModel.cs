﻿namespace Hub_Pda.Api.Model
{
    public class PdaModel
    {
        public int store_no { get; set; }
        public int pda_id { get; set; }
        public string? device_id { get; set; }
        public int? version { get; set; }

        public class PdaForm
        {
            public int store_no { get; set; }
            public int type { get; set; }
        }
    }
}
