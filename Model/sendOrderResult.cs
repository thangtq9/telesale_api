﻿namespace Hub_Pda.Api.Model
{
    public class sendOrderResult
    {
        public string trans_id { get; set; }
        public string result_code { get; set; }
        public string result_message { get; set; }
        public string? api_type { get; set; }
        public string? order_id { get; set; }
        public string? created_date { get; set; }
        public string? created_by { get; set; }
        public string? order_status { get; set; }
        public string? customer_no { get; set; }
    }
}
