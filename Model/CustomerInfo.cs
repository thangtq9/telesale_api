﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;

namespace Hub_Pda.Api.Model
{
    public class CustomerFullInfo
    {
        public string customer_id  { get; set; }
        public string? customer_no { get; set; }
        public string customer_mobile { get; set; }
        public string? buiding {  get; set; }
        public string? room { get; set; }
        public string? address { get; set; }
        public string? email { get; set; }
        public string? town { get; set; }
        public string? customer_name { get; set; }
        public int? customer_category_id { get; set; }
        public string? store_code { get; set; }
    }
}
