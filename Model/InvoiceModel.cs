﻿namespace Hub_Pda.Api.Model
{
    public class InvoiceModel
    {
        public string? storeNo { get; set; }
        public string invoiceNo { get; set; }
        public string? invoiceDate { get; set; }

    }
}
