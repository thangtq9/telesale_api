﻿namespace Hub_Pda.Api.Model
{
    public class CusDummyModel
    {
        public string store_no { get; set; }
        public string customer_no { get; set; }
        public CusDummyModel(string _store_no, string _customer_no)
        {
            this.store_no = _store_no;
            this.customer_no = _customer_no;
        }
    }
}
