﻿namespace Hub_Pda.Api.Model
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class InvoiceResponse
    {
        public string StatusCode { get; set; }
        public string message { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public Header header { get; set; }
        public List<einvoiceDetails> einvoiceDetails { get; set; }
        public custInfo custInfo { get; set; }
        public storeInfomation storeInfomation { get; set; }
    }
    public class Header
    {
        public decimal amount { get; set; }
    }
    public class einvoiceDetails
    {
        public string ArtNo { get; set; }
        public string ArtName { get; set; }
        public decimal quant { get; set; }
        public decimal amount { get; set; }
    }
    public class custInfo
    {
        public string CustomerName { get; set; }
    }

    public class storeInfomation
    {
        public string shortNameLocal { get; set; }
    }


}
