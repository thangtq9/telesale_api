﻿namespace Hub_Pda.Api.Model
{
    public class CustomerInfo
    {
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string cusId { get; set; }
        public string? Email { get; set; }
        public string? Note {  get; set; }
    }

    public class CartInfo
    {
        public string ArtNo { get; set; }
        public string ArtName { get; set; }
        public string StoreId { get; set; }
        public double Soh { get; set; }
        public int OrgPrice { get; set; }
        
        public float Quantity { get; set; }
        public double Total { get; set; }
        public int Vat_rate { get; set; }
        public int Vat_code { get; set; }
        public string? promotion_flag { get; set; } 
    }

    public class OrderInfo
    {

        public string Payment { get; set; }
        public string deliDate { get; set; }
        public string deliTime { get; set; }
        public string orderDate { get; set; }
    }
    public class TeleInfo
    {
        public string EmployeeCode {  get; set; }
        public string DisplayName { get; set; }
        public string EmployeeId { get; set; }
        
    }
    public class ReceiceOrderModel
    {
        public CustomerInfo CustomerInfo { get; set; }
        public CartInfo[] Products { get; set; }
        public OrderInfo OrderInfo { get; set; }
        public TeleInfo TeleInfo { get; set; }
        public string? cus_zalo_id { get; set; }
    }
}
