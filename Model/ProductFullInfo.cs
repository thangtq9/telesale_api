﻿namespace Hub_Pda.Api.Model
{
    public class ProductFullInfo
    {
        public string ArtNo { get; set; }

        public string ArtName { get; set; }

        public int StoreId { get; set; }

        public double Soh { get; set; }
        public int Orgprice { get; set; }
        public int status { get; set; }

        public int vat_rate { get; set; }
        public string vat_code { get; set; }
        public List<string> CampaignNames { get; set; }
    }
}
