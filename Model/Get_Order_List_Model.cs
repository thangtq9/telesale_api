﻿namespace Hub_Pda.Api.Model
{
    public class Get_Order_List_Model
    {
        public long order_id {  get; set; }
        public string order_code { get; set; }
        public string cus_name { get; set; }
        public string cus_phone { get; set; }
        public string store_code { get; set; }
        public string telesale_code { get; set; }
        public string create_date { get; set; }
        public string order_status { get; set; }
        public string order_type { get; set; }


    }
}
