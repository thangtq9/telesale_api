﻿namespace Hub_Pda.Api.Model
{
    public class ReOrderProductInfo
    {
        public string artName { get; set; }
        public string artNo { get; set; }
        public string mmunit { get; set; }
        public int orgprice { get; set; }
        public string? promotion_flag { get; set; } 
        public double quantity { get; set; }
        public double? soh { get; set; }
        public int status { get; set; }
        public int storeId { get; set; }
        public double total { get; set; }  
        public int vat_rate { get; set; }
        public int vat_code { get; set; }
        
    }
}
