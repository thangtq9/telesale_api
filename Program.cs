﻿using Hub_Pda.Api.BServices;
using Hub_Pda.Api.Context;
using Hub_Pda.Api.Helps;
using Hub_Pda.Api.Repositories;
using Hub_Pda.Api.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Text;

namespace Web.Api.Linux
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            builder.Services.AddSignalR();
            var environment_name = builder.Environment.EnvironmentName;
            IConfigurationRoot configuration = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json")
            .Build();
            builder.Services.AddDbContext<DbContextClass>();
            builder.Services.AddDbContext<CustomerDbContext>();
            builder.Services.AddDbContext<TeleSaleContextClass>();
            builder.Services.AddScoped<IArticleService, ArticleService>();
            builder.Services.AddScoped<IPdaService, PdaService>();
            builder.Services.AddScoped<IgetCusPhoneService, getCusPhoneService>();
            builder.Services.AddSingleton<OnlineUserService>();
            builder.Services.AddSingleton<ZnsApi>();
            builder.Services.AddSingleton<ApiHelper>();
            builder.Services.AddScoped<ITeleSaleService, TeleSaleService>();
            builder.Services.AddSingleton<MessageQueueService>();

            string SecurityKey = configuration["AppSettings:Secret"];
            var key11 = Encoding.ASCII.GetBytes(SecurityKey);

            builder.Services.AddAuthentication(option =>
            {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }
            ).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    IssuerSigningKey = new SymmetricSecurityKey(key11),
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            }
                );
            builder.Services.Configure<FormOptions>(o =>
            {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });

            builder.Services.AddControllers();
            builder.Services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.Converters.Add(new StringEnumConverter());
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddHostedService<MessProcessingService>();


            builder.Services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
        new CorsPolicyBuilder()
        .WithOrigins("https://uat-idata.mmvietnam.com", "https://idata.mmvietnam.com", "https://localhost:44364", "http://172.26.11.247", "https://uat-telesale.mmvietnam.com", "https://telesale.mmvietnam.com")
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials() 
        .Build());
            });

            var app = builder.Build();
            //app.UseCors(x => x
            //  .AllowAnyOrigin()
            //  .AllowAnyMethod()
            //  .AllowAnyHeader());
            app.MapGet("/", () => "net6");
            app.UseCors("CorsPolicy");

            app.UseStaticFiles();

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();
            
            app.UseAuthentication();
            app.UseAuthorization();
            app.MapHub<AuctionHub>("/auctionhub");
            app.MapControllers();

            app.Run();
        }
    }
}