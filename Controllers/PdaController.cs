﻿using Hub_Pda.Api.Entities;
using Hub_Pda.Api.Model;
using Hub_Pda.Api.Repositories;
using Hub_Pda.Api.Services;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using Web.Api.Helps;

namespace Hub_Pda.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PdaController : ControllerBaseToken
    {
        private readonly IPdaService pdaService;
        
        public PdaController(IPdaService pdaService)
        {
            this.pdaService = pdaService;
        }

        [HttpPost("liststore")]
        public DataTable GetListStore([FromBody] PdaModel.PdaForm body)
        {
            return pdaService.GetListStore(body.store_no, body.type);
        }
        
        [HttpPost("listdevice")]
        public DataTable GetPDAList([FromBody] PdaModel.PdaForm body)
        {
            return pdaService.GetPDAList(body.store_no, body.type);
            
        }
    }
}
