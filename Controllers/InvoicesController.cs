﻿using Hub_Pda.Api.Entities;
using Hub_Pda.Api.Helps;
using Hub_Pda.Api.Model;
using Hub_Pda.Api.Repositories;
using Hub_Pda.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using ServiceStack.Redis;
using System.Data;

namespace Hub_Pda.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InvoicesController : ControllerBase
    {
        private readonly ITeleSaleService tleService;
        public InvoicesController(ITeleSaleService _tleService)
        {
            this.tleService = _tleService;

        }
        [HttpPost("GetInvoice")]
        public async Task<ActionResult> GetInvoice([FromBody] InvoiceModel im)
        {
            InvoiceResponse ir = new InvoiceResponse();
            ir.StatusCode = "0000";
            try
            {
                using (DataSet ds = tleService.get_order_chanel_zns(im.invoiceNo!))
                {
                    DataTable order_info = ds.Tables[0];
                    ir.message = "Successed";


                    Data data = new Data();

                    Header he = new Header();
                    he.amount = Convert.ToDecimal(order_info.Rows[0]["amount"]);
                    data.header = he;

                    custInfo ci = new custInfo();
                    ci.CustomerName = order_info.Rows[0]["CustomerName"].ToString();
                    data.custInfo = ci;

                    storeInfomation si = new storeInfomation();
                    si.shortNameLocal = order_info.Rows[0]["shortNameLocal"].ToString();
                    data.storeInfomation = si;

                    ir.data = data;

                    List<einvoiceDetails> einvoiceDetails = new List<einvoiceDetails>();
                    if (ds.Tables[1] != null)
                    {
                        DataTable order_detail = ds.Tables[1];

                        if (order_detail != null && order_detail.Rows.Count > 0)
                        {
                            foreach (DataRow item in order_detail.Rows)
                            {
                                einvoiceDetails ed = new einvoiceDetails();
                                ed.ArtNo = item["ArtNo"].ToString();
                                ed.ArtName = item["ArtName"].ToString();
                                ed.quant = Convert.ToDecimal(item["quant"]);
                                ed.amount = Convert.ToDecimal(item["amount"]);
                                einvoiceDetails.Add(ed);
                            }
                        }
                    }
                    ir.data.einvoiceDetails = einvoiceDetails;
                }
            }
            catch (Exception ex)
            {
                ir.StatusCode = "0500";
                ir.message = ex.Message;
            }
            return Ok(ir);
        }
    }
}
