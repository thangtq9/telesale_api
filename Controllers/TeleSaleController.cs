﻿using Hub_Pda.Api.Helps;
using Hub_Pda.Api.Model;
using Hub_Pda.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Hub_Pda.Api.Model.Zalo;
using RestSharp;
using System.Net;
using ServiceStack.Redis;
using Hub_Pda.Api.Entities;
using Hub_Pda.Api.Repositories;
using System.Data;
using DocumentFormat.OpenXml.Office2010.Excel;
using OfficeOpenXml.ConditionalFormatting;

namespace Hub_Pda.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeleSaleController : ControllerBase
    {
        private readonly OnlineUserService _onlineUserService;
        protected readonly IConfiguration Configuration;
        private readonly IHubContext<AuctionHub> _hubContext;
        private readonly IRedisClientsManager _redisManager = null;
        private readonly IRedisClient _redisclient = null;
        private readonly ZnsApi znsapi;
        private readonly ApiHelper api_helper;
        private readonly IArticleService aService;
        private readonly ITeleSaleService tleService;
        private readonly MessageQueueService mpService;

        public TeleSaleController(IArticleService articleService, ApiHelper _api_helper, ITeleSaleService _tleService, IHubContext<AuctionHub> hubContext, IConfiguration configuration, OnlineUserService onlineUserService, ZnsApi _znsapi, MessageQueueService _mpService)
        {
            this.aService = articleService;
            this.tleService = _tleService;
            _hubContext = hubContext;
            Configuration = configuration;
            _onlineUserService = onlineUserService;
            znsapi = _znsapi;
            if (_redisManager == null)
            {
                try
                {
                    RedisPoolConfig _config = new RedisPoolConfig();
                    _config.MaxPoolSize = 100;
                    if (string.IsNullOrEmpty(Configuration["AppSettings:RedisPassword"]))
                        _redisManager = new RedisManagerPool(Configuration["AppSettings:RedisIP"], _config);
                    else
                    {
                        var config = new RedisEndpoint
                        {
                            Host = Configuration["AppSettings:RedisIP"],
                            Port = 6379,
                            Password = Configuration["AppSettings:RedisPassword"],
                            ConnectTimeout = 2000,
                            ReceiveTimeout = 2000,
                            SendTimeout = 2000,
                            RetryTimeout = 500,
                        };
                        _redisManager = new RedisManagerPool(config.ToString(), _config);
                    }
                    _redisclient = _redisManager.GetClient();
                }
                catch (Exception)
                {
                }
            }
            this.mpService = _mpService;
            this.api_helper = _api_helper;
        }

        [HttpPost("weekhook")]
        public async Task<ActionResult> HandleWebhookEvent([FromBody] dynamic? data)
        {
            string json_object = JsonConvert.SerializeObject(data);
            #region Log local
            string path_log = Directory.GetCurrentDirectory() + "\\Logs";
            if (!Directory.Exists(path_log))
                Directory.CreateDirectory(path_log);
            // Xử lý dữ liệu từ webhook ở đây
            string filename = "weekhook_" + DateTime.Now.ToString("yyyyMMdd");
            string Log = System.Environment.NewLine + JsonConvert.SerializeObject(data);
            FileStream _stream = null;
            StreamWriter sw = null;
            try
            {
                string Path = path_log + "/" + filename + ".log";

                if (!System.IO.File.Exists(Path))
                    _stream = System.IO.File.Create(Path);
                else
                {
                    FileInfo file = new FileInfo(Path);
                    if (file.Length > 20000000)
                    {
                        file.Delete();
                        _stream = System.IO.File.Create(Path);
                    }
                    else
                        _stream = new FileStream(Path, FileMode.Append);
                }
                sw = new StreamWriter(_stream);
                sw.Write("***" + DateTime.Now.ToString() + Environment.NewLine + Log + Environment.NewLine);
                sw.Flush();
            }
            catch (Exception ex)
            {
            }
            if (sw != null)
            {
                sw.Close();
                sw.Dispose();
            }
            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();
            }
            #endregion

            try
            {
                web_hub_mes_model? wem = JsonConvert.DeserializeObject<web_hub_mes_model>(json_object);
                if (wem != null)
                    mpService.EnqueueMess(wem);
                #region old
                //oa_list_by_store obs = new oa_list_by_store();
                //if (_redisclient != null)
                //    obs = _redisclient.Get<oa_list_by_store>("ZALO_OA_" + wem.recipient.id);
                //if (obs == null)
                //{
                //    obs = await aService.GetStoreOAInfoById(wem.recipient.id);
                //    if (_redisclient != null)
                //        _redisclient.Add("ZALO_OA_" + wem.recipient.id, obs, DateTime.Now.AddHours(12));
                //}

                //if (wem.event_name.Equals("user_send_text") && obs != null)
                //{
                //    string tele_sale_code_discuss = "";
                //    customer_zalo cz = new customer_zalo();
                //    try
                //    {

                //        if (_redisclient != null)
                //            cz = _redisclient.Get<customer_zalo>("ZALO_CUS_" + wem.sender.id + "_" + obs.oa_id);
                //        if (cz == null)
                //        {
                //            cz = await tleService.GetCusByZaloId(wem.sender.id, obs.oa_id);
                //            if (_redisclient != null)
                //                _redisclient.Add("ZALO_CUS_" + wem.sender.id + "_" + obs.oa_id, cz, DateTime.Now.AddHours(2));
                //        }
                //        tele_sale_code_discuss = cz.telesale_code;

                //        int me = await tleService.AddMess(wem.sender.id, cz.telesale_code, wem.message.text, 2);
                //    }
                //    catch (Exception ex)
                //    {
                //        var e = ex;
                //    }
                //    try
                //    {
                //        List<User_hub> users = _onlineUserService.GetOnlineUsers().ToList();
                //        if (users != null)
                //        {
                //            if (!string.IsNullOrEmpty(tele_sale_code_discuss))
                //            {
                //                User_hub? uh = (from p in users where p.employee_code == tele_sale_code_discuss select p).FirstOrDefault();
                //                if (uh != null)
                //                {
                //                    await _hubContext.Clients.Client(uh.connection_id).SendAsync("new_mes", new { cus_zalo_id = wem.sender.id, cus_name = string.IsNullOrEmpty(cz.cus_name) ? "KH mới" : cz.cus_name, text = wem.message.text, time = DateTime.Now.ToString("dd/MM HH:mm"), count_mes = 1, store_no = obs.store_no });
                //                    _onlineUserService.AddCustomerToUser(wem.sender.id, uh.employee_code);
                //                }
                //            }
                //            else
                //            {
                //                await _hubContext.Clients.All.SendAsync("new_mes", new { cus_zalo_id = wem.sender.id, cus_name = string.IsNullOrEmpty(cz.cus_name) ? "KH mới" : cz.cus_name, text = wem.message.text, time = DateTime.Now.ToString("dd/MM HH:mm"), count_mes = 1, store_no = obs.store_no });
                //            }
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
            return Ok("ok");
        }

        #region data websocket
        [HttpPost("wc_user_online")]
        public async Task<ActionResult> wc_user_online()
        {
            try
            {
                return Ok(new { user_online = _onlineUserService.GetOnlineUsers(), user_customers = _onlineUserService.GetUsersCustomerAll(), user_customers_select = _onlineUserService.GetCustomerSelectAll() });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region telesale action

        [HttpPost("get_all_cus_discuss_and_new")]
        public async Task<ActionResult> get_all_cus_discuss_and_new(string telesale_code)
        {
            try
            {
                List<customer_zalo> l_cz = await tleService.GetCusDiscussNewByTeleSale("");
                if (l_cz != null && l_cz.Count > 0)
                {
                    foreach (var item in l_cz)
                    {
                        if (!string.IsNullOrEmpty(item.telesale_code))
                            _onlineUserService.AddCustomerToUser(item.cus_zalo_id, item.telesale_code);
                    }
                }
                return Ok(l_cz);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("add_cus_discuss")]
        public async Task<ActionResult> add_cus_discuss(string cus_zalo_id, string telesale_code, string connect_id)
        {
            try
            {
                _onlineUserService.AddCustomerToUser(cus_zalo_id, telesale_code);
                customer_zalo cz = await tleService.GetCusByZaloId(cus_zalo_id,null,null);
                if (cz.status == Enum_Cus_Status.New)
                {
                    cz.cus_zalo_id = cus_zalo_id;
                    cz.telesale_code = telesale_code;
                    cz.status = Enum_Cus_Status.Inprogress;
                    cz.mes_not_read = 0;
                    await tleService.UpdateCusInfo(cz);
                }
                await _hubContext.Clients.AllExcept(new List<string> { connect_id }).SendAsync("add_cus_discuss", new { cus_zalo_id = cus_zalo_id });
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost("assign_cus_telesale")]
        public async Task<ActionResult> assign_cus_telesale(string cus_zalo_id, string cus_name, string telesale_code, string telesale_code_old)
        {
            try
            {
                _onlineUserService.AddCustomerToUser(cus_zalo_id, telesale_code, telesale_code_old);
                customer_zalo cz = new customer_zalo();
                cz.cus_zalo_id = cus_zalo_id;
                cz.telesale_code = telesale_code;
                if (!string.IsNullOrEmpty(telesale_code_old))
                    cz.telesale_code = telesale_code_old;
                await tleService.UpdateCusInfo(cz);

                string connect_id = "";
                List<User_hub> luh = _onlineUserService.GetOnlineUsers().ToList();
                if (luh != null && luh.Count > 0)
                {
                    User_hub? ul = (from p in luh where p.employee_code == telesale_code_old select p).FirstOrDefault();
                    if (ul != null)
                        connect_id = ul.connection_id;
                }

                await _hubContext.Clients.Client(connect_id).SendAsync("assign_cus_telesale", new { cus_zalo_id = cus_zalo_id, cus_name = cus_name, telesale_code = telesale_code, telesale_code_old = telesale_code_old });
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpPost("close_transaction")]
        public async Task<ActionResult> close_transaction(string cus_zalo_id)
        {
            try
            {
                customer_zalo cz = new customer_zalo();
                cz.cus_zalo_id = cus_zalo_id;
                cz.status = Enum_Cus_Status.Done;
                cz.telesale_code = "";
                await tleService.UpdateCusInfo(cz);
                return Ok("1");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost("clock_out")]
        public async Task<ActionResult> clock_out(string telesale_code)
        {
            try
            {
                List<customer_zalo> l_cz = await tleService.GetCusDiscussNewByTeleSale("");
                if (l_cz != null && l_cz.Count > 0)
                {
                    foreach (var item in l_cz)
                    {
                        if (!string.IsNullOrEmpty(item.telesale_code) && item.telesale_code == telesale_code && item.status == Enum_Cus_Status.Inprogress)
                        {
                            customer_zalo cz = new customer_zalo();
                            cz.cus_zalo_id = item.cus_zalo_id;
                            cz.status = Enum_Cus_Status.New;
                            cz.telesale_code = "";
                            await tleService.UpdateCusInfo(cz);
                        }
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion

        #region messsage

        [HttpPost("get_mes_by_zalo_cus_id")]
        public async Task<ActionResult> get_mes_by_zalo_cus_id(string telesale_code, string cus_zalo_id = "7737416472460375484", int page_number = 1)
        {
            _onlineUserService.AddCustomerToUser(cus_zalo_id, telesale_code);
            List<messages> lm = await tleService.GetMesByCusZaloId(telesale_code, cus_zalo_id, page_number);
            customer_zalo cz = await tleService.GetCusByZaloId(cus_zalo_id, null, null);
            if ((string.IsNullOrEmpty(cz.telesale_code) || cz.telesale_code == "EMPTY"))
            {
                cz.mes_not_read = 0;
                cz.status = Enum_Cus_Status.Inprogress;
                cz.telesale_code = telesale_code;
                //cz.modify_date = DateTime.Now;
                if (_redisclient != null)
                    _redisclient.Add("ZALO_CUS_" + cus_zalo_id + "_" + cz.oa_id, cz, DateTime.Now.AddMinutes(30));
                await tleService.UpdateCusInfo(cz);
            }
            else if (!string.IsNullOrEmpty(cz.telesale_code) && cz.telesale_code == telesale_code)
            {
                cz.mes_not_read = 0;
                //cz.modify_date = DateTime.Now;
                if (_redisclient != null)
                    _redisclient.Add("ZALO_CUS_" + cus_zalo_id + "_" + cz.oa_id, cz, DateTime.Now.AddMinutes(30));
                await tleService.UpdateCusInfo(cz);
            }
            string connect_id = string.Empty;
            await _hubContext.Clients.AllExcept(new List<string> { connect_id }).SendAsync("add_cus_discuss", new { cus_zalo_id = cus_zalo_id, telesale_code = telesale_code });
            _onlineUserService.AddCustomer_Select(telesale_code, cus_zalo_id);
            return Ok(lm.OrderBy(x => x.time));
        }

        [HttpPost("send_mes_to_cus")]
        public async Task<ActionResult> send_mes_to_cus([FromBody] send_mes_to_cus_model sm)
        {
            string store_test = Configuration["AppSettings:STORE_TEST"];
            if (!string.IsNullOrEmpty(store_test))
                sm.store_code = store_test;
            zns_response_model rrm = new zns_response_model();
            rrm.status_code = "000";
            rrm.msg = "Calculate";
            List<string>? l_cus_by_tele = _onlineUserService.GetCustomerByUser(sm.telesale_code);
            if (l_cus_by_tele != null)
            {
                if (l_cus_by_tele.Exists(x => x == sm.cus_zalo_id) == false)
                {
                    rrm.status_code = "005";
                    rrm.msg = "This customer is being assisted by another employee";
                    goto end;
                }
            }

            string key_accesstoken_redis = "ZNS_AC_" + sm.store_code.ToUpper();

            string access_token_old = "";
            try
            {
                messages m_tele = await tleService.AddMess(sm.cus_zalo_id, sm.cus_zalo_id+ DateTime.Now.ToString("yyyyMMddHHmmssfff"),  sm.telesale_code, sm.text, 1);
                if (m_tele != null && m_tele.message_id != null && m_tele.message_id > 0)
                {
                    if (_redisclient != null)
                    {
                        access_token_old = _redisclient.Get<string>(key_accesstoken_redis);
                    }
                    if (!string.IsNullOrEmpty(access_token_old))
                    {
                        rrm = await znsapi.Send_sms_text(rrm, access_token_old, sm.cus_zalo_id, sm.text);
                    }
                    else
                    {
                        oa_list_by_store oa = await aService.GetStoreOAInfo(sm.store_code);
                        if (oa != null)
                        {
                            RestResponse rp_at = await znsapi.GetAccessToken(oa.refresh_token, oa.app_id, oa.secret_key);
                            if (rp_at.StatusCode == HttpStatusCode.OK && rp_at.Content != null)
                            {
                                zns_token_model? ztm = JsonConvert.DeserializeObject<zns_token_model?>(rp_at.Content!);
                                if (ztm != null && !string.IsNullOrEmpty(ztm.access_token) && !string.IsNullOrEmpty(ztm.refresh_token))
                                {
                                    if (_redisclient != null)
                                    {
                                        _redisclient.Add(key_accesstoken_redis, ztm.access_token, DateTime.Now.AddHours(23));
                                    }
                                    int update_r_token = await aService.UpdateStoreOAInfo(oa, ztm.refresh_token);
                                    rrm = await znsapi.Send_sms_text(rrm, ztm.access_token, sm.cus_zalo_id, sm.text);
                                    //if (rrm.status_code == "000")
                                    //{

                                    //}
                                }
                                else
                                {
                                    rrm.status_code = "003";
                                    rrm.msg = "Thông tin access token, refresh_token bị thiếu!" + rp_at.Content;
                                }
                            }
                            else
                            {
                                rrm.status_code = "002";
                                rrm.msg = "Không lấy được access token!" + rp_at.Content;
                            }
                        }
                        else
                        {
                            rrm.status_code = "001";
                            rrm.msg = "Kho chưa đăng ký OA Zalo!";
                        }
                    }
                }
                else
                {
                    rrm.status_code = "002";
                    rrm.msg = "Không kết nối được database";
                }
            }
            catch (Exception ex)
            {
                rrm.status_code = "010";
                rrm.msg = "Lỗi!" + ex.Message;
            }

            if (rrm.status_code == "000")
                rrm.msg = DateTime.Now.ToString("dd/MM HH:ss");
            end:
            return Ok(rrm);
        }


        #endregion

        #region order action
        [HttpPost("get_order_by_zalo_id")]
        public async Task<ActionResult> get_order_by_zalo_id(string cus_zalo_id)
        {
            try
            {
                customer_zalo cz = await tleService.GetCusByZaloId(cus_zalo_id, null, null);
                if (!string.IsNullOrEmpty(cz.ecp_customer_no))
                {
                    List<orders> lm = new List<orders>();
                    CusDummyModel? cdm = (from p in api_helper.list_customer_dummy() where p.customer_no == cz.ecp_customer_no select p).FirstOrDefault();
                    if (cdm == null)
                        lm = tleService.get_order_by_cus_no(cz.ecp_customer_no).Result.Take(5).ToList();
                    else if (!string.IsNullOrEmpty(cz.cus_mobile))
                        lm = tleService.get_order_by_cus_no(cz.cus_mobile).Result.Take(5).ToList();
                    if (lm != null && lm.Count > 0)
                    {
                        foreach (var item in lm)
                        {
                            item.details = await tleService.get_order_detail(item.order_id);
                        }
                    }
                    if (lm != null && lm.Count > 0)
                        return Ok(lm.OrderByDescending(x => x.create_date_ccod));
                    else
                        return Ok(null);
                }
                else
                    return Ok(null);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("get_order_detail")]
        public async Task<ActionResult> get_order_detail(long order_id = 0, string store_no = "")
        {
            try
            {
                List<order_details> lm = await tleService.get_order_detail(order_id);
                return Ok(lm);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [HttpPost("re_calculator_order")]
        public async Task<ActionResult> re_calculator_order(string order_code,string store_code)
        {
            try
            {
                List<order_details> l_od = new List<order_details>();
                orders order_info = await tleService.get_order_by_code(order_code);
                if (order_info != null)
                {
                    l_od = await tleService.get_order_detail(order_info.order_id);
                }
                else
                    return Ok(null);
                if (l_od.Count > 0)
                {
                    string url_els = Configuration["AppSettings:ELS_URL"]!;
                    string user = Configuration["AppSettings:ELS_USER"]!;
                    string pass = Configuration["AppSettings:ELS_PW"]!;
                    string[] l_article = (from p in l_od select p.article_no).ToArray();
                    object jsonbody = new
                    {
                        _source = new string[] { "retail_prices", "art-no", "art-name" },
                        query = new
                        {
                            ids = new
                            {
                                values = l_article
                            }
                        },
                        from = 0,
                        size = 100
                    };

                    Dictionary<string, string> head = new Dictionary<string, string>();
                    head.Add("Authorization", api_helper.ConvertUserPassToBasicAuthen(user, pass)); //"Basic YXNwaXJvbjpQQHNzdzByZA=="); ; 

                    HttpResponseMessage httpResponseMessage = await api_helper.Call_API(url_els, "/_search", head, null, HttpMethod.Get, jsonbody);
                    if (httpResponseMessage != null && httpResponseMessage.StatusCode == HttpStatusCode.OK)
                    {
                        string Content = await httpResponseMessage.Content.ReadAsStringAsync();
                        var result = JsonConvert.DeserializeObject<ProductModel>(Content);
                        List<Hits> l_hit = result.hits.hits;
                        if (l_hit != null && l_hit.Count > 0)
                        {
                            List<ReOrderProductInfo> lstFilter = new List<ReOrderProductInfo>();
                            foreach (var item in l_hit)
                            {
                                ReOrderProductInfo pi = new ReOrderProductInfo();
                                pi.artNo = item._id;
                                pi.artName = item._source.artname;
                                List<RetailPrice> l_price = item._source.retail_prices;
                                if (l_price != null && l_price.Count > 0)
                                {
                                    foreach (var item_p in l_price)
                                    {
                                        if (item_p.store_id.ToString() == store_code)
                                        {
                                            pi.mmunit = item_p.mmunit;
                                            pi.status = item_p.status;
                                            pi.soh = 0;
                                            pi.promotion_flag = "";
                                            pi.vat_code = item_p.vat_code;
                                            pi.vat_rate = item_p.vat_rate;
                                            pi.orgprice = item_p.price;
                                            pi.storeId = Convert.ToInt32( store_code);

                                            order_details? od = (from p in l_od where p.article_no == item._id.ToLower() select p).FirstOrDefault();
                                            if(od != null)
                                            {
                                                pi.quantity = od.qty;
                                                pi.total = od.qty * pi.orgprice;
                                                lstFilter.Add(pi);
                                            }    
                                        }
                                    }
                                }
                            }
                            return Ok(lstFilter);
                        }
                        else
                            return NoContent();
                    }
                    else if (httpResponseMessage != null)
                    {
                        return StatusCode((int)httpResponseMessage.StatusCode, "error");
                    }
                    else
                    {
                        return NoContent();
                    }
                }
                else
                    return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

    }
}
