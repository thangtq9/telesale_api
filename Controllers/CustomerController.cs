﻿using Hub_Pda.Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Hub_Pda.Api.Context;
using Hub_Pda.Api.Model;
using Hub_Pda.Api.Repositories;

namespace Hub_Pda.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerDbContext _dbContext;
        private readonly ITeleSaleService tleService;
        private readonly IgetCusPhoneService igetCusPhoneService;

        public CustomerController(CustomerDbContext dbContext, ITeleSaleService _tleService, IgetCusPhoneService igetCusPhoneService)
        {
            _dbContext = dbContext;
            this.tleService = _tleService;
            this.igetCusPhoneService = igetCusPhoneService;
        }

        [HttpPost("GetCusInfoSql")]
        public async Task<ActionResult<customer_info>> GetCusInfoSql(string mobileNo, string? cus_zalo_id, int init_zalo)
        {
            if (init_zalo == 1)
            {
                customer_zalo cz = await tleService.GetCusByZaloId(cus_zalo_id, null, null);
                if (cz != null && !string.IsNullOrEmpty(cz.ecp_customer_no) && cz.ecp_customer_no != "EMPTY")
                {
                    var customerFullInfo = new CustomerFullInfo
                    {
                        customer_id = "",
                        customer_no = cz.ecp_customer_no,
                        customer_mobile = cz.cus_mobile ?? "",
                        address = cz.cus_deli_address,
                        email = cz.cus_email,
                        customer_name = cz.cus_name,
                        customer_category_id = (cz.cus_type == "B2B" ? 4 : 2),
                        store_code = cz.store_no
                    };
                    return Ok(customerFullInfo);
                }
                else
                    return Ok(null);
            }
            else
            {

                // Step 1: Get cusId from customer_card
                //var customerCard = await _dbContext.customer_card
                //    .FirstOrDefaultAsync(x => x.mobile_no == mobileNo);

                var customerCard = await igetCusPhoneService.GetCusAdd(mobileNo);


                if (customerCard == null)
                    return Ok(null);

                // Step 2: Use cusId to get customer_info
                //var customerInfo = await _dbContext.customer_info
                //    .FirstOrDefaultAsync(x => x.customer_id == customerCard.customer_id);

                var customerInfo = await igetCusPhoneService.GetCusType(customerCard.customer_id);

                if (customerInfo == null)
                    return Ok(null);
                var customerFullInfo = new CustomerFullInfo
                {
                    customer_id = customerInfo.customer_id,
                    customer_no = customerInfo.customer_no,
                    customer_mobile = mobileNo,
                    buiding = customerCard.building ?? null,
                    room = customerCard.room ?? null,
                    address = customerCard.address ?? null,
                    email = customerCard.email ?? null,
                    town = customerCard.town ?? null,
                    customer_name = customerInfo.customer_name ?? null,
                    customer_category_id = customerInfo.customer_catogory_id ?? null,
                    store_code = customerInfo.home_store
                };

                if (!string.IsNullOrEmpty(cus_zalo_id))
                {
                    customer_zalo cz = new customer_zalo();
                    cz.cus_zalo_id = cus_zalo_id;
                    cz.ecp_customer_no = customerInfo.customer_no;
                    cz.cus_name = customerInfo.customer_name;
                    cz.cus_mobile = mobileNo;
                    cz.cus_email = customerCard.email ?? null;
                    cz.cus_deli_address = customerCard.address ?? null;
                    cz.cus_type = customerInfo.customer_catogory_id + "" == "4" ? "B2B" : "B2C";
                    await tleService.UpdateCusInfo(cz);
                }

                return Ok(customerFullInfo);
            }
        }
    }
}
