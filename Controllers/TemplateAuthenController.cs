﻿using DocumentFormat.OpenXml.Spreadsheet;
using Hub_Pda.Api.Context;
using Hub_Pda.Api.Entities;
using Hub_Pda.Api.Helps;
using Hub_Pda.Api.Model;
using Hub_Pda.Api.Model.Zalo;
using Hub_Pda.Api.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using RestSharp;
using ServiceStack.Web;
using System.Data;
using System.Globalization;
using System.Net;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using TeleSale.Api.Model;

namespace Web.Api.Controllers
{
    [Route("api/[controller]")]
    public class TemplateAuthenController : ControllerBase
    {
        private readonly ITeleSaleService tleService;
        private readonly TeleSaleContextClass _dbContext;
        private static readonly HttpClient Client = new HttpClient();
        protected readonly IConfiguration Configuration;
        private readonly ZnsApi znsapi;
        private readonly ApiHelper api_helper;

        protected string ccod_url = "";
        protected string ccod_x_api_key = "";
        protected string ccod_key = "";

        protected string ecp_url = "";
        protected string ecp_x_api_key = "";
        protected string ecp_key = "";

        protected string els_url = "";
        protected string els_user = "";
        protected string els_pass = "";

        public TemplateAuthenController(ApiHelper _api_helper, ZnsApi _znsapi, IConfiguration configuration, ITeleSaleService _tleService, TeleSaleContextClass dbContext)
        {
            _dbContext = dbContext;
            this.tleService = _tleService;
            this.Configuration = configuration;
            this.znsapi = _znsapi;
            this.api_helper = _api_helper;

            this.ccod_url = Configuration["AppSettings:CCOD_URL"];
            this.ccod_x_api_key = Configuration["AppSettings:CCOD_x_api_key"];
            this.ccod_key = Configuration["AppSettings:CCOD_key"];

            this.ecp_url = Configuration["AppSettings:ECP_URL"];
            this.ecp_x_api_key = Configuration["AppSettings:ECP_x_api_key"];
            this.ecp_key = Configuration["AppSettings:ECP_key"];


            this.els_url = Configuration["AppSettings:ELS_URL"];
            this.els_user = Configuration["AppSettings:ELS_USER"];
            this.els_pass = Configuration["AppSettings:ELS_PW"];
        }

        //**********************************************//
        //***********BẮT ĐẦU API TELESALE***************//
        //**********************************************//

        [HttpPost("get_order_detail")]
        public async Task<ActionResult> get_order_detail(long order_id = 0)
        {
            try
            {
                List<order_details> lm = await tleService.get_order_detail(order_id);
                return Ok(lm);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpPost("send_order")]
        public async Task<ActionResult> send_order([FromBody] ReceiceOrderModel data)
        {
            try
            {
                var os = await tleService.get_order_seq();
                SendOrderModel som = new SendOrderModel();
                som.order_id = "TLS" + os;
                som.po_id = "TLS" + os;
                som.created_date = data.OrderInfo.orderDate;
                som.created_by = data.TeleInfo.DisplayName;
                som.sale_channel = "3";
                som.order_status = "10";
                som.vat_indicator = "2";
                som.customer_no = data.CustomerInfo.cusId;
                som.customer_name = data.CustomerInfo.Name;
                som.representative = data.CustomerInfo.Name;
                som.receiver_name = data.CustomerInfo.cusId;
                som.receiver_phone = data.CustomerInfo.Phone;
                som.delivery_email = data.CustomerInfo.Email ?? null;
                som.invoice_address = data.CustomerInfo.Address;
                som.delivery_address = data.CustomerInfo.Address;
                som.internal_comment = data.CustomerInfo.Note ?? null;
                som.picking_comment = data.CustomerInfo.Note ?? null;
                som.Invoice_comment = data.CustomerInfo.Note ?? null;
                som.delivery_comment = data.CustomerInfo.Note ?? null;
                som.sale_order_source = 1;
                foreach (var item in data.Products)
                {
                    som.store_code = item.StoreId;
                }
                som.payment_type = "POD_";
                som.fulfillment_type = "1";
                som.invoice_address_id = "null";
                som.fiscal_no = "0401880552-001";
                som.delivery_office_phone = null;
                som.delivery_fax = null;
                som.delivery_date = data.OrderInfo.deliDate;
                som.delivery_time_from = data.OrderInfo.deliTime;
                if (DateTime.TryParseExact(som.delivery_time_from, "HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsedDeliveryTimeFrom))
                {
                    DateTime adjustedDeliveryTimeFrom = parsedDeliveryTimeFrom.AddMinutes(-15);
                    som.delivery_time_from = adjustedDeliveryTimeFrom.ToString("HH:mm");
                }
                som.delivery_time_to = data.OrderInfo.deliTime;
                if (DateTime.TryParseExact(som.delivery_time_to, "HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsedDeliveryTimeTo))
                {
                    DateTime adjustedDeliveryTimeTo = parsedDeliveryTimeTo.AddMinutes(15);
                    som.delivery_time_to = adjustedDeliveryTimeTo.ToString("HH:mm");
                }
                som.send_order_mail_flag = null;
                som.send_order_email = null;

                som.total_price_ex_vat = 0;
                som.total_discount_ex_vat = 0;
                som.total_vat = 0;
                som.payment_list = new List<PaymentList>();
                double Totalprice = 0;
                foreach (var cartItem in data.Products)
                {
                    Totalprice += cartItem.Total;
                }
                PaymentList payment_list = new PaymentList
                {
                    payment_code = (data.OrderInfo.Payment.ToLower() == "cash") ? "_POD" : data.OrderInfo.Payment,
                    payment_name = GetPaymentName(data.OrderInfo.Payment.ToLower()),
                    payment_no = null,
                    prepaid_flag = (data.OrderInfo.Payment.ToLower() == "cash") ? "0" : "1"
                };
                static string GetPaymentName(string paymentMethod)
                {
                    if (paymentMethod == "cash")
                    {
                        return "Tiền mặt";
                    }
                    else if (paymentMethod == "decdmomo")
                    {
                        return "Chuyển khoản qua Momo";
                    }
                    else if (paymentMethod == "decdvibb")
                    {
                        return "Chuyển khoản qua ngân hàng";
                    }
                    else
                    {
                        return "Chuyển khoản qua VNPAY";
                    }
                }
                som.payment_list.Add(payment_list);
                som.order_items = new List<OrderItem>();
                int currentSeq = 1;

                foreach (var cartItem in data.Products)
                {
                    OrderItem order_items = new OrderItem
                    {
                        item_seq = currentSeq,
                        article_code = cartItem.ArtNo,
                        order_qty = cartItem.Quantity,
                        remark = null,
                        price_code = "74",
                        discount_code = null,
                        discount_event = null,

                        vat_rate = cartItem.Vat_rate,
                        vat_code = cartItem.Vat_code,


                        item_discount_ex_vat = 0,
                        qty_discount = 0,
                    };

                    double B = (double)cartItem.Vat_rate / 100;
                    double A = cartItem.OrgPrice / (1 + B);
                    order_items.fulfillment_price = Math.Round(A);
                    order_items.total_price_ex_vat = Math.Round(order_items.fulfillment_price * order_items.order_qty);
                    order_items.total_price_in_vat = Math.Round(Math.Round(A) * order_items.order_qty * (1 + B));
                    order_items.vat_after_disc = Math.Round(order_items.total_price_in_vat - order_items.total_price_ex_vat);
                    payment_list.amount = 0;

                    som.order_items.Add(order_items);
                    foreach (var item in som.order_items)
                    {
                        payment_list.amount += (decimal)item.total_price_in_vat;
                    }
                    order_items.unit_price_ex_vat = Math.Round(A);
                    som.total_price_ex_vat += order_items.total_price_ex_vat;
                    som.total_discount_ex_vat += order_items.item_discount_ex_vat;
                    som.total_vat += order_items.vat_after_disc;
                    currentSeq++;
                }

                som.order_item_discount = new List<OrderItemDiscount>();
                OrderItemDiscount order_item_discount = new OrderItemDiscount
                {
                    item_seq = null,
                    qty_discount = null,
                    discount_ex_vat = null,
                    reward_qty = null,
                    discount_type = null,
                    discount_code = null,
                    discount_rate = null,
                    discount_event = null
                };
                som.order_item_discount.Add(order_item_discount);
                var orderApiData = new
                {
                    orderModel = som
                };
                var round_total_price_ex_vat = Math.Round(som.total_price_ex_vat);
                som.total_price_ex_vat = round_total_price_ex_vat;
                var round_total_vat = Math.Round(som.total_vat);
                som.total_vat = round_total_vat;
                string jsonData = JsonConvert.SerializeObject(som);
                var options = new RestClientOptions(ccod_url)
                {
                    MaxTimeout = -1,
                };
                HttpClientHandler clientHandler = new HttpClientHandler();
                HttpClient client1 = new HttpClient(clientHandler);

                var client = new HttpClient();
                var request = new HttpRequestMessage(HttpMethod.Post, ccod_url + "/api/ccod/order/v1/create");
                request.Headers.Add("x-api-key", ccod_x_api_key);
                string hash = "";
                using (HMACSHA256 hmac = new HMACSHA256(Encoding.UTF8.GetBytes(ccod_key)))
                {
                    byte[] hashBytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(jsonData));
                    hash = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
                }
                request.Headers.Add("x-req-sig", hash);
                request.Content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string _result = response.Content.ReadAsStringAsync().Result;
                sendOrderResult? result = JsonConvert.DeserializeObject<sendOrderResult>(_result);
                if (result != null && result.result_code == "0000")
                {
                    try
                    {
                        var orders = new orders
                        {
                            order_code = result.order_id!,
                            ccod_order_code = som.order_id,
                            trans_id = result.trans_id,
                            cus_zalo_id = data.cus_zalo_id,
                            ecp_customer_no = som.customer_no,
                            cus_name = som.customer_name,
                            cus_phone = data.CustomerInfo.Phone,
                            cus_email = data.CustomerInfo.Email ?? null,
                            cus_deli_address = data.CustomerInfo.Address,
                            delivery_date = DateTime.ParseExact(data.OrderInfo.deliDate, "yyyy-MM-dd", CultureInfo.InvariantCulture),
                            delivery_time = data.OrderInfo.deliTime,
                            payment_method = data.OrderInfo.Payment,
                            comment = data.CustomerInfo.Note ?? null,
                            store_code = som.store_code,
                            telesale_code = data.TeleInfo.EmployeeCode,
                            total_amount = Convert.ToDecimal(payment_list.amount),
                            create_date = DateTime.Now,
                            create_date_ccod = result.created_date,
                            order_type = !string.IsNullOrEmpty(data.cus_zalo_id) ? "zalo" : "telephone",
                            order_status = "Done"
                        };
                        _dbContext.orders.Add(orders);
                        await _dbContext.SaveChangesAsync();
                        foreach (var product in data.Products)
                        {
                            var orderDetail = new order_details
                            {
                                order_id = orders.order_id,
                                article_no = product.ArtNo,
                                article_name = product.ArtName,
                                qty = Math.Round(product.Quantity, 2),
                                vat_code = product.Vat_code,
                                vat_rate = product.Vat_rate
                            };
                            decimal vat = (decimal)product.Vat_rate / 100;
                            decimal unit_price_no_vat = product.OrgPrice / (1 + vat);

                            orderDetail.unit_price_ex_vat = Math.Round(unit_price_no_vat);
                            orderDetail.total_amount_ex_vat = Math.Round(orderDetail.unit_price_ex_vat * (decimal)product.Quantity);
                            orderDetail.unit_price_in_vat = Math.Round(orderDetail.unit_price_ex_vat + orderDetail.unit_price_ex_vat * orderDetail.vat_rate / 100);
                            orderDetail.total_amount_in_vat = Math.Round(orderDetail.unit_price_ex_vat * (1 + vat) * (decimal)product.Quantity, 0);
                            orderDetail.is_promotion = product.promotion_flag == "Y" ? 1 : 0;
                            _dbContext.order_details.Add(orderDetail);
                        }

                        await _dbContext.SaveChangesAsync();

                    }
                    catch (Exception ex)
                    {
                        return StatusCode(500, $"Internal server error: {ex.Message}");
                    }
                    os += 1;
                    var orderSeq = new order_seq
                    {
                        o_seq = os
                    };
                    var entity = await _dbContext.order_seq.FirstOrDefaultAsync();
                    _dbContext.order_seq.Remove(entity);
                    _dbContext.order_seq.Add(orderSeq);
                    await _dbContext.SaveChangesAsync();

                    try
                    {
                        customer_zalo cz = new customer_zalo();
                        cz.ecp_customer_no = som.customer_no;
                        cz.cus_name = som.customer_name;
                        cz.cus_mobile = data.CustomerInfo.Phone;
                        cz.cus_email = data.CustomerInfo.Email ?? null;
                        cz.cus_deli_address = data.CustomerInfo.Address;
                        await tleService.UpdateCusInfo(cz);
                    }
                    catch (Exception)
                    {

                        throw;
                    }

                    try
                    {
                        if (string.IsNullOrEmpty(data.cus_zalo_id))
                        {
                            string zns_api = Configuration["AppSettings:ZNSAPI"]!;
                            string store_test = Configuration["AppSettings:STORE_TEST"];
                            string bu_test = Configuration["AppSettings:BU_TEST"];
                            using (DataSet ds = tleService.get_order_chanel_zns(result.order_id))
                            {
                                DataTable dt = ds.Tables[0];
                                int amount = 0;
                                string store_name = dt.Rows[0]["shortNameLocal"].ToString();
                                long purchasedate = Convert.ToInt32(dt.Rows[0]["create_date"].ToString());
                                string customer_name = dt.Rows[0]["CustomerName"].ToString();
                                string phone = dt.Rows[0]["cus_phone"].ToString();
                                string store_code = dt.Rows[0]["store_code"].ToString();
                                if (!string.IsNullOrEmpty(store_test))
                                    store_code = store_test;
                                string bu = dt.Rows[0]["bu"].ToString();
                                if (!string.IsNullOrEmpty(bu_test))
                                    bu = bu_test;
                                string order_no = dt.Rows[0]["order_code"].ToString();
                                amount = (int)Convert.ToDecimal(dt.Rows[0]["amount"].ToString());
                                zns_response_model zrm = await znsapi.zns_noti(zns_api, order_no, amount, store_name, purchasedate, customer_name, phone, store_code, bu);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    result.result_message = "Success";
                    return Ok(result);
                }
                else
                {
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
        [HttpPost("get_dummy_cus_by_store")]
        public async Task<ActionResult> get_dummy_cus_by_store([FromBody] string data)
        {
            try
            {
                CusDummyModel cdm = (from p in api_helper.list_customer_dummy() where p.store_no == data select p).FirstOrDefault();
                if (cdm != null)
                    return Ok(cdm.customer_no);
                else
                    return Ok("1");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpPost("get_product_info")]
        //public async Task<ActionResult> get_product_info(int? home_store, string? searchTerm, int searchFrom)
        //{
        //    if (searchTerm == null) { return Ok("5"); }
        //    else
        //    {
        //        try
        //        {
        //            var lstFilter = await GetFilteredProductListAsync(home_store, searchTerm, searchFrom, 10);

        //            if (lstFilter.Count > 0)
        //            {
        //                foreach (var Product in lstFilter)
        //                {
        //                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        //                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        //                    HttpClientHandler clientHandler_promote = new HttpClientHandler();
        //                    clientHandler_promote.ServerCertificateCustomValidationCallback = (sender, cert, chain, SslPolicyErrors) => { return true; };
        //                    HttpClient client_promote = new HttpClient(clientHandler_promote);
        //                    //Response.Headers.Add("Access-Control-Allow-Origin", "*");
        //                    //Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        //                    //Response.Headers.Add("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Origin, Authorization");


        //                    var currentDate = DateTime.Now.ToString("yyyy-MM-dd");
        //                    var request_promote = new HttpRequestMessage(HttpMethod.Post, "http://172.26.24.115:4201/api/gold/pricetaginfo?arg_date=" + currentDate + "&arg_site=" + home_store + "&arg_code=" + Product.ArtNo);
        //                    request_promote.Headers.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IntcInVzZXJuYW1lXCI6XCJpdHRlc3RcIixcInBhc3N3b3JkXCI6XCIxMjNcIn0iLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJbe1wicGVybWlzc3Rpb25faWRcIjoxfSx7XCJwZXJtaXNzdGlvbl9pZFwiOjJ9XSIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJbe1wibWVudV9pZFwiOjEsXCJtZW51X25hbWVcIjpcIlJlcG9ydFwiLFwibWVudV9yb3V0ZXJcIjpcIi9yZXBvcnQvcmVwb3J0ZGF0YVwifV0iLCJuYmYiOjE2OTcwOTUyNjQsImV4cCI6MTcyODYzMTI2NH0.R3j-Fo-6CZDrJBFU_yzmXUbNiScrfohkDl9sPm75Tsc");
        //                    var response_promote = await client_promote.SendAsync(request_promote);
        //                    response_promote.EnsureSuccessStatusCode();
        //                    var resultTemp_promote = await response_promote.Content.ReadAsStringAsync();
        //                    var result_promote = JsonConvert.DeserializeObject<List<ProductInfo>>(resultTemp_promote);
        //                    if (result_promote.Count > 0)
        //                    {
        //                        Product.promotion_flag = result_promote[0].promotion_flag;
        //                    }

        //                }
        //                return Ok(lstFilter);
        //            }
        //            else
        //            {
        //                return Ok("1");
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return Ok("1");
        //            // Đã return 1 để hiển thị không tìm thấy kết quả
        //            //return StatusCode(500, ex.Message);
        //        }
        //    }
        //}
        //private async Task<List<ProductInfo>> GetFilteredProductListAsync(int? home_store, string? searchTerm, int searchFrom, int targetSize)
        //{
        //    var lstFilter = new List<ProductInfo>();

        //    using (var client = new HttpClient())
        //    {
        //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        //        ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        //        HttpClientHandler clientHandler = new HttpClientHandler();
        //        clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, SslPolicyErrors) => { return true; };

        //        int remainingProducts = targetSize;
        //        int lastProductPlace = searchFrom;
        //        while (remainingProducts > 0)
        //        {
        //            var request = new HttpRequestMessage(HttpMethod.Get, "http://172.26.16.109:9200/articles_uat/_search");
        //            request.Headers.Add("Authorization", "Basic YXNwaXJvbjpQQHNzdzByZA==");
        //            var json = "{\"query\" : {\"match\" : {\"art-name\":\"" + searchTerm + "\"}},\"from\" : " + searchFrom + ", \"size\" : " + remainingProducts + "}";
        //            var content = new StringContent(json, null, "application/json");
        //            request.Content = content;

        //            var response = await client.SendAsync(request);
        //            response.EnsureSuccessStatusCode();

        //            var resultTemp = await response.Content.ReadAsStringAsync();
        //            var result = JsonConvert.DeserializeObject<ProductModel>(resultTemp);
        //            List<Hits> lstData = result.hits.hits;

        //            foreach (Hits hit in lstData)
        //            {
        //                lastProductPlace += 1;
        //                ProductInfo pi = new ProductInfo();
        //                pi.ArtNo = hit._source.artno;
        //                pi.ArtName = hit._source.artname;
        //                Stock stock_current = new Stock();
        //                //List<int> storeIds = new List<int>();
        //                //List<double> sohs = new List<double>();

        //                    pi.Soh = 0;


        //                RetailPrice retailPrice = new RetailPrice();
        //                List<int> price = new List<int>();
        //                List<int> status = new List<int>();
        //                if (hit._source.retail_prices != null && hit._source.retail_prices.Count != 0)
        //                {
        //                    foreach (RetailPrice retailprice in hit._source.retail_prices)
        //                    {
        //                        if (retailprice.store_id == home_store)
        //                        {
        //                            retailPrice = retailprice;
        //                            break;
        //                        }
        //                    }
        //                }
        //                if (retailPrice != null && retailPrice.store_id > 0)
        //                {
        //                    pi.StoreId = retailPrice.store_id;
        //                    pi.Orgprice = retailPrice.price;
        //                    pi.status = retailPrice.status;
        //                    pi.vat_rate = retailPrice.vat_rate;
        //                    pi.vat_code = retailPrice.vat_code;
        //                    pi.mmunit = retailPrice.mmunit;
        //                    lstFilter.Add(pi);
        //                }
        //            }
        //            // Lọc sản phẩm với product.status = 2
        //            lstFilter = lstFilter.Where(product => product.status != 2).ToList();
        //            remainingProducts = targetSize - lstFilter.Count;

        //            searchFrom += lstData.Count;
        //        }
        //        return lstFilter.Take(targetSize).ToList();
        //    }
        //}
        public async Task<ActionResult> get_product_info(int? home_store, string? searchTerm)
        {
            if (searchTerm == null) { return Ok("5"); }
            else
            {
                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                    HttpClientHandler clientHandler = new HttpClientHandler();
                    clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, SslPolicyErrors) => { return true; };
                    HttpClient client = new HttpClient(clientHandler);


                    var request = new HttpRequestMessage(HttpMethod.Get, els_url + "/_search");
                    request.Headers.Add("Authorization", api_helper.ConvertUserPassToBasicAuthen(els_user, els_pass));
                    var json = "{\"query\" : {\"match\" : {\"art-name\":\"" + searchTerm + "\"}},\"from\" : 0, \"size\" : 20}";
                    var content = new StringContent(json, null, "application/json");
                    request.Content = content;
                    var response = await client.SendAsync(request);
                    response.EnsureSuccessStatusCode();
                    Console.WriteLine(await response.Content.ReadAsStringAsync());



                    var resultTemp = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<ProductModel>(resultTemp);
                    List<Hits> lstData = result.hits.hits;
                    lstData = lstData
                        .OrderByDescending(product => product._score)
                        .ToList();
                    List<ProductInfo> lstStore = new List<ProductInfo>();
                    List<ProductInfo> lstFilter = new List<ProductInfo>();



                    bool foundProduct = false;
                    foreach (Hits hit in lstData)
                    {
                        ProductInfo pi = new ProductInfo();
                        pi.ArtNo = hit._source.artno;
                        pi.ArtName = hit._source.artname;
                        Stock stock_current = new Stock();
                        pi.Soh = 0;
                        RetailPrice retailPrice = new RetailPrice();
                        List<int> price = new List<int>();
                        List<int> status = new List<int>();
                        if (hit._source.retail_prices != null && hit._source.retail_prices.Count != 0)
                        {
                            foreach (RetailPrice retailprice in hit._source.retail_prices)
                            {
                                if (retailprice.store_id == home_store)
                                {
                                    retailPrice = retailprice;
                                    break;
                                }
                            }
                        }
                        if (retailPrice != null && retailPrice.store_id > 0)
                        {
                            pi.StoreId = retailPrice.store_id;
                            pi.Orgprice = retailPrice.price;
                            pi.status = retailPrice.status;
                            pi.vat_rate = retailPrice.vat_rate;
                            pi.vat_code = retailPrice.vat_code;
                            pi.mmunit = retailPrice.mmunit;
                            lstFilter.Add(pi);
                            lstFilter = lstFilter
                            .OrderBy(product => product.status == 2)
                            .ThenBy(product => product.status)
                            .ToList();
                            foundProduct = true;
                        }
                    }
                    if (foundProduct)
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                        ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                        HttpClientHandler clientHandler_promote = new HttpClientHandler();
                        clientHandler_promote.ServerCertificateCustomValidationCallback = (sender, cert, chain, SslPolicyErrors) => { return true; };
                        HttpClient client_promote = new HttpClient(clientHandler_promote);
                        Response.Headers.Add("Access-Control-Allow-Origin", "*");
                        Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
                        Response.Headers.Add("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Origin, Authorization");
                        foreach (ProductInfo Product in lstFilter)
                        {
                            if (Product.status != 2)
                            {
                                var currentDate = DateTime.Now.ToString("yyyy-MM-dd");
                                var request_promote = new HttpRequestMessage(HttpMethod.Post, "http://172.26.24.115:4201/api/gold/pricetaginfo?arg_date=" + currentDate + "&arg_site=" + home_store + "&arg_code=" + Product.ArtNo);
                                request_promote.Headers.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IntcInVzZXJuYW1lXCI6XCJpdHRlc3RcIixcInBhc3N3b3JkXCI6XCIxMjNcIn0iLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJbe1wicGVybWlzc3Rpb25faWRcIjoxfSx7XCJwZXJtaXNzdGlvbl9pZFwiOjJ9XSIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJbe1wibWVudV9pZFwiOjEsXCJtZW51X25hbWVcIjpcIlJlcG9ydFwiLFwibWVudV9yb3V0ZXJcIjpcIi9yZXBvcnQvcmVwb3J0ZGF0YVwifV0iLCJuYmYiOjE2OTcwOTUyNjQsImV4cCI6MTcyODYzMTI2NH0.R3j-Fo-6CZDrJBFU_yzmXUbNiScrfohkDl9sPm75Tsc");
                                var response_promote = await client.SendAsync(request_promote);
                                response_promote.EnsureSuccessStatusCode();
                                var resultTemp_promote = await response_promote.Content.ReadAsStringAsync();
                                var result_promote = JsonConvert.DeserializeObject<List<ProductInfo>>(resultTemp_promote);
                                if (result_promote.Count > 0)
                                {
                                    Product.promotion_flag = result_promote[0].promotion_flag;
                                }
                            }
                        }
                        return Ok(lstFilter);
                    }
                    else
                    {
                        return Ok("1");
                    }
                }
                catch (Exception ex)
                {
                    return Ok("1");
                    //Đã return 1 để hiển thị không tìm thấy kết quả
                    //return StatusCode(500, ex.Message);
                }
            }
        }

        [HttpPost("get_product_infov2")]
        public async Task<ActionResult> get_product_infov2(int? home_store, string? searchTerm)
        {
            try
            {
              if (home_store == null || string.IsNullOrEmpty(searchTerm))
                    return Ok("5");
                string prefix = searchTerm;
                if (searchTerm.IndexOf(" ") > 0)
                    prefix = searchTerm.Split(' ')[0];


                object jsonbody = api_helper.template_search_article(prefix, searchTerm, home_store.Value);
                Dictionary<string, string> head = new Dictionary<string, string>();
                head.Add("Authorization", api_helper.ConvertUserPassToBasicAuthen(els_user, els_pass)); //"Basic YXNwaXJvbjpQQHNzdzByZA=="); ; 
                HttpResponseMessage response = await api_helper.Call_API(els_url, "/_search", head, null, HttpMethod.Get, jsonbody);

                var resultTemp = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<ProductModel>(resultTemp);
                List<Hits> lstData = result!.hits.hits;

                object jsonbody_contain = api_helper.template_search_article_contain(prefix, searchTerm, home_store.Value);
                response = await api_helper.Call_API(els_url, "/_search", head, null, HttpMethod.Get, jsonbody_contain);

                resultTemp = await response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<ProductModel>(resultTemp);
                List<Hits> lstData_contain = result!.hits.hits;

                if ((lstData == null || lstData.Count == 0) && (lstData_contain == null || lstData_contain.Count == 0))
                    return Ok("1");

                List<ProductInfo> lstFilter = new List<ProductInfo>();
                int _index_api = 1;
                bool foundProduct = false;
                if (lstData != null && lstData.Count > 0)
                {
                    foreach (Hits hit in lstData)
                    {
                        ProductInfo pi = new ProductInfo();
                        pi.ArtNo = hit._source.artno;
                        pi.ArtName = hit._source.artname;

                        if (hit != null && hit._source.stock != null && hit._source.stock.Count > 0)
                        {
                            Stock? s = (from p in hit._source.stock where p.store_id == home_store select p).FirstOrDefault();
                            if (s != null && s.soh != null)
                                pi.Soh = s.soh;
                            else
                                pi.Soh = 0;
                        }
                        else
                            pi.Soh = 0;
                        RetailPrice? retailPrice = (from p in hit._source.retail_prices where p.store_id == home_store select p).FirstOrDefault();
                        if (retailPrice != null && retailPrice.store_id > 0)
                        {
                            pi.StoreId = retailPrice.store_id;
                            pi.Orgprice = retailPrice.price;
                            pi.status = retailPrice.status == 2 ? 2 : 1;
                            pi.vat_rate = retailPrice.vat_rate;
                            pi.vat_code = retailPrice.vat_code;
                            pi.mmunit = retailPrice.mmunit;
                            pi.index_api = _index_api; _index_api++;
                            if (pi.ArtName.ToLower().IndexOf(searchTerm) == 0)
                                pi.index_start_key = 1;
                            else
                                pi.index_start_key = 100;
                            lstFilter.Add(pi);
                            foundProduct = true;
                        }
                    }
                }

                if (lstData_contain != null && lstData_contain.Count > 0)
                {
                    foreach (Hits hit in lstData_contain)
                    {
                        ProductInfo pi = new ProductInfo();
                        pi.ArtNo = hit._source.artno;
                        pi.ArtName = hit._source.artname;

                        if (hit != null && hit._source.stock != null && hit._source.stock.Count > 0)
                        {
                            Stock? s = (from p in hit._source.stock where p.store_id == home_store select p).FirstOrDefault();
                            if (s != null && s.soh != null)
                                pi.Soh = s.soh;
                            else
                                pi.Soh = 0;
                        }
                        else
                            pi.Soh = 0;
                        RetailPrice? retailPrice = (from p in hit._source.retail_prices where p.store_id == home_store select p).FirstOrDefault();
                        if (retailPrice != null && retailPrice.store_id > 0 && retailPrice.price > 1)
                        {
                            pi.StoreId = retailPrice.store_id;
                            pi.Orgprice = retailPrice.price;
                            pi.status = retailPrice.status ==2?2:1;
                            pi.vat_rate = retailPrice.vat_rate;
                            pi.vat_code = retailPrice.vat_code;
                            pi.mmunit = retailPrice.mmunit;
                            pi.index_api = _index_api; _index_api++;
                            if (pi.ArtName.ToLower().IndexOf(searchTerm) == 0)
                                pi.index_start_key = 1;
                            else
                                pi.index_start_key = 100;
                            if (lstFilter != null && lstFilter.Count > 0)
                            {
                                ProductInfo? pi_check = (from p in lstFilter where p.ArtNo == pi.ArtNo select p).FirstOrDefault();
                                if (pi_check == null)
                                    lstFilter.Add(pi);
                            }
                            else
                                lstFilter!.Add(pi);
                            foundProduct = true;
                        }
                    }
                }

                if (foundProduct)
                {
                    List<ProductInfo> _lstFilter = lstFilter.OrderBy(x => x.status).ThenBy(x => x.index_start_key).ThenBy(x => x.index_api).ToList();
                    return Ok(_lstFilter);
                }
                else
                    return Ok("1");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("get_promotion_by_product")]
        public async Task<ActionResult> get_promotion_by_product(string home_store, string article_no)
        {
            try
            {
                object jsonrequest = new
                {
                    page_size = 10,
                    page_no = 1,
                    dnr_promotion_year = DateTime.Now.Year.ToString(),
                    effect_store = home_store,
                    dnr_promotion_status="AP",
                    barcode= article_no,
                    effective_flag="T",
                    effect_bu="12"
                };
                string jsonData = JsonConvert.SerializeObject(jsonrequest);


                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, SslPolicyErrors) => { return true; };
                HttpClient client = new HttpClient(clientHandler);

                var request = new HttpRequestMessage(HttpMethod.Post, ecp_url + "/ecp/dnr/v1/search");
                request.Headers.Add("x-api-key", ecp_x_api_key);
                string hash = "";
                using (HMACSHA256 hmac = new HMACSHA256(Encoding.UTF8.GetBytes(ecp_key)))
                {
                    byte[] hashBytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(jsonData));
                    hash = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
                }
                request.Headers.Add("x-req-sig", hash);
                request.Content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string _result = response.Content.ReadAsStringAsync().Result;
                ecp_promotion_model? result = JsonConvert.DeserializeObject<ecp_promotion_model>(_result);
                if(result != null && result.data != null && result.data.Count > 0)
                    return Ok(1);
                return Ok(0);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("get_order_list")]
        public async Task<ActionResult<orders>> get_order_list()
        {
            var order_list = await tleService.get_order_list();
            if (order_list == null)
            {
                return Ok(null);
            }
            var ols = new List<Get_Order_List_Model>();
            foreach (var order in order_list)
            {
                var ol = new Get_Order_List_Model
                {
                    order_id = order.order_id,
                    order_code = order.order_code,
                    cus_name = order.cus_name,
                    cus_phone = order.cus_phone,
                    store_code = order.store_code,
                    telesale_code = order.telesale_code,
                    create_date = order.create_date_ccod,
                    order_status = order.order_status,
                    order_type = order.order_type
                };
                ols.Add(ol);
            }
            return Ok(ols);
        }
        [HttpPost("get_filter_list")]
        public async Task<ActionResult> get_filter_list(
            [FromQuery] DateTime? fromDate,
            [FromQuery] DateTime? toDate,
            [FromQuery] string? source,
            [FromQuery] string? telesaleId,
            [FromQuery] string? storeId,
            [FromQuery] string? cusPhone,
            [FromQuery] string? status,
            [FromQuery] string? feedback
        )
        {
            var query = _dbContext.orders.AsQueryable();
            if (fromDate.HasValue)
            {
                query = query.Where(x => x.create_date >= fromDate.Value);
            }
            if (toDate.HasValue)
            {
                query = query.Where(x => x.create_date <= toDate.Value);
            }
            if (!string.IsNullOrEmpty(source))
            {
                query = query.Where(x => x.order_type == source);
            }
            if (!string.IsNullOrEmpty(telesaleId))
            {
                query = query.Where(x => x.telesale_code == telesaleId);
            }
            if (!string.IsNullOrEmpty(storeId))
            {
                query = query.Where(x => x.store_code == storeId);
            }
            if (!string.IsNullOrEmpty(cusPhone))
            {
                query = query.Where(x => x.cus_phone == cusPhone);
            }
            if (!string.IsNullOrEmpty(status))
            {
                query = query.Where(x => x.order_status == status);
            }
            if (!string.IsNullOrEmpty(feedback))
            {
                query = query.Where(x => x.feedback == feedback);
            }
            var filteredOrders = await query.ToListAsync();
            return Ok(filteredOrders);
        }

    }

}
