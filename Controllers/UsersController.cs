﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Web.Api.Model;

namespace Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        protected readonly IConfiguration Configuration;
        public UsersController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        [HttpPost("authenticate")]
        public async Task<ActionResult> authenticate([FromBody] UserModel user)
        {
            if(user == null || string.IsNullOrEmpty(user.username))
            {
                return BadRequest("Vui lòng nhập đầy đủ thông tin mật khẩu");
            }
            else
            {

                List<PermisstionButonModel> lp = new List<PermisstionButonModel>();
                PermisstionButonModel p = new PermisstionButonModel();
                p.permisstion_id = 1;
                lp.Add(p);

                p = new PermisstionButonModel();
                p.permisstion_id = 2;
                lp.Add(p);


                List<MenuModel> lm = new List<MenuModel>();
                MenuModel m = new MenuModel();
                m.menu_id = 1; m.menu_name = "Report"; m.menu_router = "/report/reportdata";
                lm.Add(m);

                string SecurityKey = Configuration["AppSettings:Secret"];
                var now = DateTime.UtcNow;
                var claims = new Claim[]
                { 
                    new Claim(ClaimTypes.NameIdentifier, JsonConvert.SerializeObject(user)),
                    new Claim(ClaimTypes.Role, JsonConvert.SerializeObject(lp)),
                    new Claim(ClaimTypes.Name, JsonConvert.SerializeObject(lm)),
                };
                var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecurityKey));
                var jwt = new JwtSecurityToken(
                    claims: claims,
                    notBefore: now,
                    expires: now.Add(TimeSpan.FromDays(1)),
                    signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
                    );

                var encodeJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                var responeJson = new
                {
                    token = encodeJwt,
                    expires_in = TimeSpan.FromDays(1).TotalDays.ToString() + " days",
                    msg = "OK",
                    username = user.username,
                    password = user.password
                };
                return Ok(responeJson);
            }    
            
        }
    }
}
