﻿using System.ComponentModel.DataAnnotations;

namespace Hub_Pda.Api.Entities
{
    public class order_seq
    {
        [Key]
        public int o_seq { get; set; }
    }
}
