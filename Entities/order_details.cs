﻿using System.ComponentModel.DataAnnotations;

namespace Hub_Pda.Api.Entities
{
    public class order_details
    {
        [Key]
        public long od_id { get; set; }
        public long order_id { get; set; }
        public string article_no { get; set; }
        public string article_name { get; set; } = "";
        public double qty { get; set; }
        public decimal unit_price_ex_vat { get; set; }
        public decimal unit_price_in_vat { get; set; }
        public int vat_rate { get; set; }
        public int vat_code { get; set; }

        public decimal total_amount_in_vat { get; set; }
        public decimal total_amount_ex_vat { get; set; }
        public int is_promotion { get; set; } = 0;
    }
}
