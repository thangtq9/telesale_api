﻿using System.ComponentModel.DataAnnotations;

namespace Hub_Pda.Api.Entities
{
    public class oa_list_by_store
    {
        [Key]
        public int id { get; set; } = 0;
        public string bu { get; set; } = "";
        public string store_no { get; set; } = "";
        public string refresh_token { get; set; } = "";
        public string? access_token { get; set; } = "";
        public string app_id { get; set; } = "";
        public string secret_key { get; set; } = "";
        public string template_id { get; set; } = "";
        public DateTime create_date { get; set; }
        public DateTime? modifi_date { get; set; } = null;
        public string oa_id { get; set; } = "";
    }
}
