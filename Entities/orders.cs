﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hub_Pda.Api.Entities
{
    public class orders
    {
        [Key]
        public long order_id { get; set; }
        public string order_code { get; set; }
        public string ccod_order_code { get; set; }
        public string? trans_id { get; set; }
        public string? cus_zalo_id { get; set; }
        public string? ecp_customer_no { get; set; }
        public string cus_name { get; set; }
        public string cus_phone { get; set; }
        public string cus_deli_address { get; set; }
        public DateTime delivery_date { get; set; }
        public string delivery_time { get; set; }
        public string payment_method { get; set; }
        public string? comment { get; set; }
        public string store_code { get; set; }
        public string telesale_code { get; set; }
        public decimal total_amount { get; set; }
        public string order_status { get; set; }
        public string order_type { get; set; }
        public DateTime create_date { get; set; }
        public string? create_date_ccod { get; set; }
        public string? cus_email { get; set; }
        public string? feedback {  get; set; }

        [NotMapped]
        public List<order_details> details { get; set; }
        
    }
}
