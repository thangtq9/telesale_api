﻿using System.ComponentModel.DataAnnotations;

namespace Hub_Pda.Api.Entities
{
    public class messages
    {
        [Key]
        public long message_id { set; get; }
        public string msg_id { set; get; }
        public string cus_zalo_id { set; get; }
        public string? telesale_code { set; get; }
        public string? text { set; get; } = "";
        public int user_chat { set; get; } = 1;
        public DateTime? time { set; get; } = DateTime.Now;
    }
}
