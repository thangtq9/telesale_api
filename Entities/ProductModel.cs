﻿using Hub_Pda.Api.Model;
using Newtonsoft.Json;

namespace Hub_Pda.Api.Entities
{
    public class Hits
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public double _score { get; set; }
        public Source _source { get; set; }
        public Total? total { get; set; }
        public double? max_score { get; set; }
        public List<Hits> hits { get; set; }
    }
    public class RetailPrice
    {
        public int store_id { get; set; }
        public int perm_price { get; set; }
        public int price_code { get; set; }
        public int vat_code { get; set; }
        public string mmunit { get; set; }
        public int price { get; set; }
        public DateTime modified_date { get; set; }
        public int status { get; set; }
        public int vat_rate { get; set; }
    }
    public class ProductModel
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public Shards _shards { get; set; }
        public Hits hits { get; set; }
    }
    public class Sales
    {
        public List<object>? in_day { get; set; }
    }
    public class Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int skipped { get; set; }
        public int failed { get; set; }
    }
    public class Stock
    {
        public int? store_id { get; set; }
        public double? soh { get; set; }
    }
    public class Source
    {
        [JsonProperty("art-no")]    
        public string artno { get; set; }

        [JsonProperty("art-name")]
        public string artname { get; set; }
        public int status { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime modifiedAt { get; set; }
        public List<Stock>? stock { get; set; }
        public List<object> assortments { get; set; }
        public Sales sales { get; set; }
        public List<RetailPrice> retail_prices { get; set; }
    }
    public class Total
    {
        public int value { get; set; }
        public string relation { get; set; }
    }  
    public class ProductInfo
    {
        
        public string ArtNo { get; set; }

        public string ArtName { get; set; }

        public int StoreId { get; set; }
        public string mmunit { get; set; }
        public double? Soh { get; set; }
        public int Orgprice { get; set; }
        public int status { get; set; }

        public int vat_rate { get; set; }
        public int vat_code { get; set; }
        public string? promotion_flag { get; set; }
        public int index_api { get; set; }
        public int index_start_key { get; set; }

    }

    public class ProductList
    {
        public List<ProductInfo> data { get; set; }
    }
}
