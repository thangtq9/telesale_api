﻿using System.ComponentModel.DataAnnotations;

namespace Hub_Pda.Api.Entities
{
    public class customer_card
    {
        [Key]
        public string customer_id { get; set; }
        public string? card_no { get; set; }
        public string? building { get; set; } 
        public string? room { get; set; }
        public string? address { get; set; } 
        public string? mobile_no { get; set; } 
        public string? email { get; set; } 
        public string? town { get; set; } 

    }
}
