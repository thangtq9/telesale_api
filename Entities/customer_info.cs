﻿using System.ComponentModel.DataAnnotations;

namespace Hub_Pda.Api.Entities
{
    public class customer_info
    {
        [Key]
        public string customer_id { get; set; } 
        public string customer_no { get; set; }
        public string? customer_name { get; set; } 
        public int? customer_catogory_id { get; set; } 
        public string? home_store { get; set; } 
        public DateTime? create_date { get; set; } 
        

    }
}
