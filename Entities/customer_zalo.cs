﻿using System.ComponentModel.DataAnnotations;

namespace Hub_Pda.Api.Entities
{
    public class customer_zalo
    {
        [Key]
        public string cus_zalo_id { get; set; }
        public string? cus_name { get; set; } = "EMPTY";
        public string? ecp_customer_no { get; set; } = "EMPTY";
        public string? cus_mobile { get; set; } = "EMPTY";
        public string? cus_type { get; set; } = "EMPTY";
        public string? cus_deli_address { get; set; } = "EMPTY";
        public string? cus_email { get; set; } = "EMPTY";
        public DateTime? create_date { get; set; }
        public string? store_no { get; set; } = "EMPTY";
        public string? status { get; set; } = "EMPTY";
        public string? oa_id { get; set; } = "EMPTY";
        public DateTime? modify_date { get; set; }
        public int? mes_not_read { get; set; } = -1;
        public string? text { get; set; } = "";
        public string? telesale_code { get; set; } = "EMPTY";
    }
}
