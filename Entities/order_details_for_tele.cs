﻿using Microsoft.Identity.Client;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hub_Pda.Api.Entities
{
    public class order_details_for_tele
    {
        [Key]
        public int id { get; set; }
        public string order_id { get; set; }
        public string article_no { get; set; }
        public string article_name { get; set; }
        public float qty { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal unit_price_ex_vat { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal unit_price_in_vat { get; set; }
        public int vat_rate { get; set; }
        public int vat_code { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal total_amount_ex_vat { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal total_amount_in_vat { get; set; }

    }
}
