﻿using Hub_Pda.Api.Entities;
using Hub_Pda.Api.Model;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

using Microsoft.Extensions.Configuration;
using System.Data;

namespace Hub_Pda.Api.Context
{
    public class CustomerDbContext : DbContext
    {
        protected readonly IConfiguration Configuration;
        public CustomerDbContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(Configuration.GetConnectionString("CusConnection"));
        }
        public DbSet<customer_card> customer_card { get; set; }
        public DbSet<customer_info> customer_info { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<customer_card>().HasKey(c => c.customer_id);
            modelBuilder.Entity<customer_card>()
            .Property(c => c.customer_id)
            .IsRequired();
            
            modelBuilder.Entity<customer_info>().HasKey(d => d.customer_id);
            modelBuilder.Entity<customer_info>()
            .Property(d => d.customer_id)
            .IsRequired();
            
            base.OnModelCreating(modelBuilder);
        }
        public DataSet ExecuteCusDataSet(string procedureName, int commandTimeoutInSeconds, params SqlParameter[] parameters)
        {
            var dataSet = new DataSet();
            using (var connection = Database.GetDbConnection())
            {
                try
                {
                    if (connection.State != ConnectionState.Open) connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = procedureName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = commandTimeoutInSeconds;
                        using (var adapter = new SqlDataAdapter((SqlCommand)command))
                        {
                            adapter.Fill(dataSet);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                    GC.Collect();
                }
            }
            return dataSet;
        }
    }
}
