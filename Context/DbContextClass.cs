﻿using Hub_Pda.Api.Entities;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Hub_Pda.Api.Model;

namespace Hub_Pda.Api.Context
{
    public class DbContextClass : DbContext
    {
        protected readonly IConfiguration Configuration;
            
        public DbContextClass(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
        }

        //public DbSet<Article> Article { get; set; }
        public DbSet<oa_list_by_store> oa_list_by_store { get; set; }

        public DataTable ExecuteDataTable(string procedureName, int commandTimeoutInSeconds, params SqlParameter[] parameters)
        {
            var dataTable = new DataTable();
            using (var connection = Database.GetDbConnection())
            {
                try
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = procedureName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = commandTimeoutInSeconds;

                        using (var adapter = new SqlDataAdapter((SqlCommand)command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                    GC.Collect();
                }
            }
            return dataTable;
        }
        public DataSet ExecuteDataSet(string procedureName, int commandTimeoutInSeconds, params SqlParameter[] parameters)
        {
            var dataSet = new DataSet();
            using (var connection = Database.GetDbConnection())
            {
                try
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = procedureName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = commandTimeoutInSeconds;

                        using (var adapter = new SqlDataAdapter((SqlCommand)command))
                        {
                            adapter.Fill(dataSet);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                    GC.Collect();
                }
            }
            return dataSet;
        }
        public int ExecuteNonQuery(string procedureName, int commandTimeoutInSeconds, params SqlParameter[] parameters)
        {
            using (var connection = Database.GetDbConnection())
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = procedureName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = commandTimeoutInSeconds;
                        command.Transaction = transaction;
                        int var = command.ExecuteNonQuery();
                        transaction.Commit();
                        return var;
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                    }
                    throw;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                        GC.Collect();
                    }
                }
            }
        }
    }
    

}
